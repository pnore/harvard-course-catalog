        var mapLat, mapLong, classInfo;
        
		function setCoords(mlat, mlong){
			mapLat = mlat;
			mapLong = mlong;
		}

		function setClassInfo(class_info){
			classInfo = class_info;
		}        

        function initialize() {
          //var myLatlng = new google.maps.LatLng(42.376364, -71.116637); 
		  var myLatlng = new google.maps.LatLng(mapLat, mapLong);
          var mapOptions = {
            zoom: 16,
            center: myLatlng, /*new google.maps.LatLng(42.376364, -71.116637),*/
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }
          
          var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
          
          var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              title: classInfo
          });            
          
          
        }
        
        function loadScript() {
          var script = document.createElement("script");
          script.type = "text/javascript";
          script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBIeeYf5txsQ2syRS37Izh_yqWS9Ue1RqE&sensor=false&callback=initialize";
          document.body.appendChild(script);
        } 