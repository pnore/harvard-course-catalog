<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:pmn="http://www.myernore.com"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="pmn xsl xs">
    <xsl:import href="common.xsl"/>
<xsl:output
  method="xml"
  encoding="utf-8"
  omit-xml-declaration="yes"
  doctype-public="http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd"
  doctype-system="-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
  />
    <xsl:param name="crs_grp"/>
    <xsl:strip-space elements="*" />

    <xsl:variable name="courses" select="/fas_courses/course[course_group/@code = $crs_grp][@offered='Y' and course_level/@code='P']"/>

    <xsl:variable name="page_title" >
        <xsl:value-of select="$courses[1]/course_group"/>
    </xsl:variable>

    <xsl:variable name="crs_grp_course_table_headings">
        <tr>
            <th>Course Number</th> 
            <th>Terms Offered</th> 
            <th>Course Title</th>
        </tr>
    </xsl:variable>

  <xsl:template match="fas_courses">
    <h2 id="top-group">
        <xsl:value-of select="$courses[1]/course_group"/>
    </h2>
    <table>
      <xsl:copy-of select="$crs_grp_course_table_headings" />
      <xsl:apply-templates select="$courses" mode="table-row">
          <xsl:sort select="course_number/num_int" data-type="number"/>
          <xsl:sort select="course_number/num_char"/>
      </xsl:apply-templates>
    </table>
  </xsl:template>

  <xsl:template match="course" mode="table-row">
    <tr>
        <td><xsl:apply-templates select="course_number"/></td>
        <td><xsl:apply-templates select="term"/></td>
        <td><xsl:apply-templates select="title"/></td>
    </tr>
  </xsl:template>

  <xsl:template match="course_number">
    <a href="{concat('course-',../@cat_num,'.html')}">
        <xsl:apply-templates mode="common" select="."/>
    </a>
  </xsl:template>

</xsl:stylesheet>
