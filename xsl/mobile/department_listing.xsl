<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="2.0" 
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="pmn xsl xs">
    <xsl:import href="common.xsl"/>
    <xsl:variable name="title">Course Catalog - Department Listing</xsl:variable>
    <xsl:param name="href" select="concat('course_groups','/','CG-')"></xsl:param>
    <xsl:param name="url"/>
<xsl:output
  method="xml"
  encoding="utf-8"
  omit-xml-declaration="yes"
  doctype-public="http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd"
  doctype-system="-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
  />

   
    <xsl:strip-space elements="*" />

    <xsl:variable name="page_title" as="xs:string" >
        <xsl:text>Department and Course Group Listing</xsl:text>
    </xsl:variable>


  <xsl:template match="fas_courses">
    <h2>List of Departments and their Course Groups</h2>
    <ul>
        <!-- todo: add button to optionally turn off 
            restriction for courses in current year and 
            courses primarily for undergraduates -->
            <!--
        <xsl:for-each-group 
            select="course[@offered='Y' and course_level/@code='P']" 
            group-by="department/@code">
            <xsl:sort select="department/@code"/>
            -->
        <xsl:for-each-group select="course" group-by="department/@code">
            <li>
                <xsl:choose>
                    <xsl:when test="
                        count(
                            distinct-values(
                                current-group()/course_group/@code
                        )) eq 1">                            
                        <a>
                            <xsl:attribute name="href">                            
                                <xsl:value-of select="
                                    concat($relpath_crs_grp,
                                    '&amp;course_group=',course_group/@code 
                                    )"/>
                            </xsl:attribute>
                            <xsl:value-of select="department/dept_short_name"/>                            
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        <h3><xsl:value-of select="department/dept_short_name"/></h3>
                        <ul>
                            <xsl:for-each-group
                                select="current-group()"
                                group-by="course_group/@code">
                                <xsl:sort select="course_group/@code"/>
                                <li>                                    
                                    <a>
                                        <xsl:attribute name="href">                                    
                                            <xsl:value-of select="
                                                concat(
                                                $baselink,
                                                $medium,
                                                '/course_group/',
                                                @blocknr,
                                                '/?',
                                                'course_group=',
                                                current-grouping-key())"/>
                                        </xsl:attribute>                                        
                                        <xsl:value-of select="concat(course_group,'(',
                                            count(distinct-values(//fas_courses/course[course_group/@code = current-grouping-key()])),
                                            ')')"/>                                     
                                        
                                    </a>
                                </li>
                            </xsl:for-each-group>
                        </ul>
                    </xsl:otherwise>
                </xsl:choose>
            </li>
        </xsl:for-each-group>
    </ul>
  </xsl:template>
</xsl:stylesheet>
