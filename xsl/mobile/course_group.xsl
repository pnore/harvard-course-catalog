<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:pmn="http://www.myernore.com"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="pmn xsl xs">
    <xsl:import href="common.xsl"/>
<xsl:output
  method="xml"
  encoding="utf-8"
  omit-xml-declaration="yes"
  doctype-public="http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd"
  doctype-system="-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
  />
    <xsl:param name="course_group"/>
    <xsl:strip-space elements="*" />

<!--
    <xsl:variable name="courses" select="/fas_courses/course[course_group/@code = $crs_grp][@offered='Y' and course_level/@code='P']"/>
    -->

    <xsl:variable name="page_title" >
        <xsl:text>Course Group :</xsl:text>
       <!-- <xsl:value-of select="data/fas_courses"></xsl:value-of>-->        
        <xsl:value-of select="distinct-values(data/fas_courses/course[course_group/@code = $course_group]/course_group)"/>        
    </xsl:variable>

    <xsl:variable name="crs_grp_course_table_headings">
        <tr>
            <th>Course Number</th> 
            <th>Terms Offered</th> 
            <th>Course Title</th>
        </tr>
    </xsl:variable>

    <xsl:template match="fas_courses">
      <h3><a><xsl:attribute name="href">
            <xsl:value-of select="
                concat($baselink,$medium,
                '/department_listing/filter?'
                )"/>
        </xsl:attribute>
         Department
      </a></h3>
      <br></br>
      <h1>
        <xsl:value-of select="$page_title"/>
        <xsl:text> | Page </xsl:text>
        <xsl:value-of select="$blocknr"/>
      </h1> 
      <div>
        <ul style="list-style-type:none;display:inline;">
          <xsl:if test="number($blocknr) gt 1">
            <li style="display:inline; padding:15px;">
              <a href="{concat(
                $relpath,
                number($blocknr)-1,
                '/?',$querystring)}">prev</a>
            </li>
          </xsl:if>
          <!-- invalid slot number for local variable-->
          <!--
        <xsl:for-each select="for $i in 1 to 10 return $i">
        yep
        </xsl:for-each>
        -->
          <xsl:for-each-group select="/data/fas_courses/course/@blocknr" group-by=".">
            <li style="display:inline; padding:5px;">
              <xsl:choose>
                <xsl:when test="current-group()[1]=$blocknr">
                  <xsl:value-of select="current-group()[1]"/>
                </xsl:when>
                <xsl:otherwise>
                  <a href="{concat(
                    $relpath
                    ,current-group()[1]
                    ,'/?'
                    ,$querystring)}">
                    <xsl:value-of select="current-group()[1]"/>
                  </a>
                </xsl:otherwise>
              </xsl:choose>
            </li>
          </xsl:for-each-group>
          <xsl:if test="number($blocknr) le $lastblock">
            <li style="display:inline; padding:15px;">
              <a href="{concat(
                $relpath,
                number($blocknr)+1,'/?',
                $querystring)}">next</a></li>
          </xsl:if>
        </ul>
      </div>
<!--      <xsl:value-of select="data/fas_courses"></xsl:value-of>
      <xsl:for-each-group 
        select="/data/fas_courses/course/course_group" 
        group-by="@code">
        <xsl:if test="last() gt 1">
          <a href="#course_group-{current-grouping-key()}">
            <xsl:value-of select="."/>
          </a>
          <xsl:if test="position() != last()">
            <xsl:text> | </xsl:text>
          </xsl:if>
       </xsl:if>
      </xsl:for-each-group>-->
    <xsl:for-each-group 
      select="/data/fas_courses/course[@blocknr=$blocknr]" 
      group-by="course_group/@code">
    <h2 id="{concat('course_group-',current-grouping-key())}">
        <xsl:value-of select="current-group()[1]/course_group"/>
    </h2>
    <table>
      <xsl:copy-of select="$crs_grp_course_table_headings" />
      <xsl:for-each select="current-group()">
        <xsl:apply-templates select="." mode="table-row"/>
      </xsl:for-each>
    </table>
    </xsl:for-each-group>
  </xsl:template>

  <xsl:template match="course" mode="table-row">
      <tr>
        <td><xsl:apply-templates select="course_number"/></td>
        <td><xsl:apply-templates select="term"/></td>
        <td><xsl:apply-templates select="title"/></td>
    </tr>
  </xsl:template>

  <xsl:template match="course_number">
      
      <a>
          <xsl:attribute name="href">
              <xsl:value-of select="
                  concat($baselink,$medium,
                  '/course_detail/filter?',$querystring,'&amp;','cat_num=',
                  ../@cat_num 
                  )"/>
          </xsl:attribute>
          <xsl:apply-templates mode="common" select="."/>
      </a>
  </xsl:template>

</xsl:stylesheet>
