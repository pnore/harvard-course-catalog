<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:pmn="http://www.myernore.com"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="pmn xsl xs">
  <xsl:import href="common.xsl"/>
<xsl:output
  method="xml"
  encoding="utf-8"
  omit-xml-declaration="yes"
  doctype-public="http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd"
  doctype-system="-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
/>
    <xsl:strip-space elements="*" />
    
    <xsl:param name="dept"/>
    <xsl:param name="cat_num"/>
    <xsl:variable name="courses" select="/data/fas_courses/course[@cat_num=$cat_num]"/>

    <xsl:variable name="page_title" >
    Course Detail: 
      <xsl:value-of select="/data/fas_courses/course[@cat_num=$cat_num]/title"/>
    </xsl:variable>
  <xsl:variable name="course_group" > 
    <xsl:value-of select="/data/fas_courses/course[@cat_num=$cat_num]/course_group/@code"/>
  </xsl:variable>

  <xsl:template match="fas_courses">
    
    <h3>
    <a><xsl:attribute name="href">
      <xsl:value-of select="
        concat($baselink,$medium,
        '/department_listing/filter?'
        )"/>
    </xsl:attribute>
      Department
    </a>
   -->
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="
          concat($baselink,$medium,
          '/course_group/filter?',
          '&amp;course_group=',$course_group
          )"/>
      </xsl:attribute>
      <xsl:value-of select="/data/fas_courses/course[@cat_num=$cat_num]/course_group"/>
      
    </a>
    </h3>
    <xsl:apply-templates select="/data/fas_courses/course"/>
  </xsl:template>

  <xsl:template match="course">
    <xsl:apply-templates 
    select="title
        , course_number
        , @cat_num
        , faculty_text
        , credit 
        , course_level
        , prerequisites
        , description 
        , notes
        "/>
  </xsl:template>

  <xsl:template match="title">
      <h2><xsl:apply-templates/></h2> 
  </xsl:template>

  <xsl:template match="course_number">
    <h1>
        <!-- why doesn't xsl:apply-imports with default mode work here?-->
        <xsl:apply-templates select="." mode="common"/>
        <xsl:text>. </xsl:text>
        <xsl:value-of select="../title"/>
    </h1>
  </xsl:template>

  <xsl:template match="@cat_num">
    <p>
        <xsl:text>Catalog Number: </xsl:text>
        <xsl:value-of select="."/>
    </p>
  </xsl:template>

  <xsl:template match="faculty_text">
    <p><em><xsl:apply-templates/></em></p>
  </xsl:template>

  <xsl:template match="credit">
    <p>
    <xsl:apply-templates/>
    <xsl:apply-templates select="../term, ../meeting_text, ../@offered"/>
    </p>
  </xsl:template>

  <xsl:template match="@offered">
  <xsl:if test=".='N'">
    <xsl:text> (</xsl:text>
     <em>
       <xsl:text>next offered: </xsl:text>
       <xsl:value-of select="../@next_year_offered"/> 
     </em>
    <xsl:text>)</xsl:text>
  </xsl:if>
  </xsl:template>

  <xsl:template match="term">
    <xsl:text> (</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>). </xsl:text>
  </xsl:template>

  <xsl:template match="meeting_text">
    <xsl:value-of select="translate(., ';.', '')"/>
    <xsl:text>.</xsl:text>
  </xsl:template>

  <xsl:template match="course_level">
    <p>
    <xsl:apply-templates/>
    <xsl:text> / </xsl:text>
    <xsl:apply-templates select="../course_type"/>
    </p>
  </xsl:template>

  <xsl:template match="prerequisites[string-length(text()) gt 0]">
    <p>
        <strong>
            <xsl:text>Prerequisites</xsl:text>
            <xsl:text>: </xsl:text>
        </strong>
        <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="instructor_approval_required[text() = 'Y']">
    <xsl:text>(INSTRUCTOR APPROVAL REQUIRED)</xsl:text>
  </xsl:template>

  <xsl:template match="description">
    <p class="course description"><xsl:apply-templates/></p>
  </xsl:template>

  <xsl:template match="notes[string-length(text()) gt 0]">
    <p class="course note">
        <em><xsl:text>Note: </xsl:text></em>
    <xsl:apply-templates/></p>
  </xsl:template>

</xsl:stylesheet>
