<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    version="2.0" 
    exclude-result-prefixes="xsl xs">
<xsl:output
  method="xml"
  encoding="utf-8"
  omit-xml-declaration="yes"
  doctype-public="http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd"
  doctype-system="-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
  />
  <xsl:import href="../common.xsl"/>
  <xsl:param name="url"/>
  <xsl:param name="querystring"/>
  <xsl:param name="baselink"/>
  <xsl:param name="medium"/>
  <xsl:param name="view"/>
  <xsl:param name="blocknr"/>
  
  
  <xsl:variable name="default_predicates" select="'offered=Y&amp;level=P'"/>
  <xsl:variable name="lastblock" select="number(/data/fas_courses/course[@lastblock='1'][1]/@blocknr)"/>
  <xsl:variable name="qs" select="replace($querystring,$default_predicates,'')"/>
  <!--<xsl:variable name="relpath" select="concat(
  $baselink
  ,$medium
  ,'/',$view
  ,'/filter?'
  ,$default_predicates)"/>-->
  <xsl:variable name="relpath" select="concat($baselink,$medium,'/',$view,'/')"/>
  <xsl:variable name="relpath_crs_grp" select="concat(
  $baselink
  ,$medium
  ,'/course_group/filter?'
  ,$default_predicates)"/>
  <xsl:variable name="relpath_crs_dtl" select="concat(
  $baselink
  ,$medium
  ,'/course_detail/filter?'
  ,$default_predicates)"/>
  
<xsl:strip-space elements="*" />
  <xsl:variable name="site_title" as="xs:string">
    <xsl:text>Harvard University</xsl:text>
  </xsl:variable>
  <xsl:variable name="page_title" as="xs:string">Harvard University</xsl:variable>
  <xsl:variable name="evencolor">#efefef</xsl:variable>
  <xsl:variable name="oddcolor">#cccccc</xsl:variable>
  <xsl:key name="course_code" match="course" use="normalize-space(@code)"/>
  <xsl:key name="dept_code" match="course" use="normalize-space(department/@code)"/>
  <xsl:key name="course_group_code" match="course" use="normalize-space(course_group/@code)"/>
  <xsl:variable name="root" select="/"/>
  <xsl:variable name="breadcrumb_separator">
    <span class="separator">&#x2192;</span>
  </xsl:variable>
  <xsl:variable name="courses" select="/fas_courses/course"/>
  <!--
  <xsl:variable name="courses" select="/fas_courses/course[course_group/@code = $crs_grp][@offered='Y' and course_level/@code='P']"/>
  -->
  <xsl:variable name="num_courses" select="count($courses/@cat_num)" as="xs:integer"/>
  <xsl:variable name="num_depts" select="count(distinct-values($courses/course_group/@code))"/>

  <xsl:template match="raw">
      <!-- remove - -->
  </xsl:template>
  <xsl:template match="/">
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width"/>
        <title>
          <xsl:value-of select="concat($page_title, $site_title)"/>
        </title>
        <link rel="stylesheet" href="{$baselink}css/mobile-style.css" type="text/css" media="screen, projection" />
        <xsl:call-template name="style"/>
      </head>
      <body>
        <xsl:call-template name="header"/>
         <div class="content" id="content">
            <xsl:apply-templates />
            <xsl:call-template name="footer"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="breadcrumb">
    <div class="breadcrumb">
        <a href="departments-all.html">
            <xsl:text>Departments</xsl:text>
        </a>
        <xsl:choose>
            <xsl:when test="$num_depts eq 1">
                <xsl:copy-of select="$breadcrumb_separator"/>
                <a href="{concat('department-',$courses[1]/course_group/@code,'.html')}">
                    <xsl:value-of select="$courses[1]/course_group"/>
                </a>
            </xsl:when>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="$num_courses eq 1">
                <xsl:copy-of select="$breadcrumb_separator"/>
                <a href="{concat('course-',$courses[1]/@cat_num,'.html')}">
                    <xsl:apply-templates select="$courses[1]/course_number" mode="common"/>
                </a>
            </xsl:when>
        </xsl:choose>
    </div>
  </xsl:template>

  <xsl:template name="header">
    <div id="header">
      <a href="{$baselink}">
        <!--<img width="72" height="85" id="harvard_logo_img" src="/cocoon/assignment3/images/harvard_shield.png" alt="harvard logo"/>-->
        <img src="{$baselink}img/header-course-catalog.png" width="320" height="57" alt="course_catalog" /> 
      </a>
      <h1>
        <xsl:value-of select="$site_title"/>
        <br/>
        <xsl:value-of select="$page_title"/>
      </h1>
      <a class="site_version_link" href="{$baselink}xhtml/{$view}/filter?{$querystring}">Switch to Non-Mobile Site</a>
      <div class="clear"></div>
    </div>
  </xsl:template>

  <xsl:template name="footer">
    <div class="footer">
  <p>Site templating by Navy Team</p>
  <p>
  Data used with permission from
  <a href="http://www.registrar.fas.harvard.edu/courses-exams/courses-instruction?cat=ugrad&amp;subcat=courses">Harvard FAS Registrar's Office</a>
  </p>
  </div>
  </xsl:template>

  <xsl:template name="style">
    <style type="text/css">
    tr.evenrow {background-color:<xsl:value-of select="$evencolor"/>;} 
    tr.oddrow {background-color:<xsl:value-of select="$oddcolor"/>;} 
    </style>
  </xsl:template>

<!--
  <xsl:template match="course_number" mode="common">
    <xsl:value-of select="
    concat(
        if (../@offered='N') then '[' else ''
        , ../course_group
        , ' '
        , num_int
        , num_char
        , if (../@offered='N') then ']' else ''
        , if (../instructor_approval_required/text()='Y') then '*' else ''
        )"/>
    </xsl:template>
    -->

    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

</xsl:stylesheet>
