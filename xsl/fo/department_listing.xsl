<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
		
	<xsl:import href="common.xsl"/>
	<xsl:import href="styles.xsl"/>		
<!--  	
	<xsl:variable name="title">Course Catalog - Department Listing</xsl:variable>
	<xsl:param name="href" select="concat('course_groups','/','CG-')"></xsl:param>
	<xsl:param name="url"/>
	<xsl:param name="querystring"/>
	<xsl:param name="baselink"/>
	<xsl:param name="medium"/>
	<xsl:param name="view"/>
	<xsl:variable name="_space" select="' '" />
-->	


	<xsl:template match="fas_courses">
		<fo:block xsl:use-attribute-sets="docblock toc-padding" break-after="page">
			<xsl:for-each-group select="//department" group-by="@code">
				<xsl:sort select="@code"/>
				<fo:block text-align-last="justify" xsl:use-attribute-sets="docblock dept-header">
					<fo:basic-link internal-destination="{generate-id()}">
						<xsl:value-of select="dept_long_name"/>
						(<xsl:value-of select="current-grouping-key()"/>)
					</fo:basic-link>
					<fo:leader leader-pattern="dots"/>
					<fo:page-number-citation ref-id="{generate-id()}"/>
					<xsl:call-template name="course-groups"/>
				</fo:block>
			</xsl:for-each-group>
		</fo:block>		
		<fo:block>
			<xsl:for-each-group select="//department" group-by="@code">
				<xsl:sort select="@code"/>
				<fo:block id="{generate-id()}" xsl:use-attribute-sets="docblock" break-after="page">
					<fo:block xsl:use-attribute-sets="dept-header-listing">
						<xsl:value-of select="upper-case(dept_long_name)"/>
						(<xsl:value-of select="current-grouping-key()"/>)
					</fo:block>
					<xsl:call-template name="course-groups-listing"/>
				</fo:block>
			</xsl:for-each-group>			
		</fo:block>
	</xsl:template>
	
	<xsl:template name="course-groups">
		<xsl:for-each-group select="//course/department[@code=current-grouping-key()]/../course_group" group-by="@code">		
			<xsl:sort select="text()"/>
			<fo:block text-align-last="justify" xsl:use-attribute-sets="docblock">
				<fo:basic-link color="grey" internal-destination="{generate-id()}">
					<xsl:value-of select="text()"/>
					<!--(<xsl:value-of select="current-grouping-key()"/>)-->
				</fo:basic-link>
				<fo:leader leader-pattern="dots"/>
				<fo:page-number-citation ref-id="{generate-id()}"/>
			</fo:block>
		</xsl:for-each-group>
	</xsl:template>
	

	<xsl:template name="course-groups-listing">
		<xsl:for-each-group select="//course/department[@code=current-grouping-key()]/../course_group" group-by="@code">		
			<xsl:sort select="text()"/>
			<fo:block xsl:use-attribute-sets="docblock group-header" id="{generate-id()}" break-after="page">
				<fo:block xsl:use-attribute-sets="gh-border">
					<xsl:value-of select="text()"/>
					<xsl:text> Group</xsl:text> &#187;
					<!--(<xsl:value-of select="current-grouping-key()"/>)-->
				</fo:block>
				<xsl:call-template name="courses"/>
			</fo:block>
		</xsl:for-each-group>
	</xsl:template>
	
	<xsl:template name="courses">
		<!--<xsl:value-of select="current-grouping-key()"/>-->
		<xsl:apply-templates select="//course/course_group[@code=current-grouping-key()]/.."/>
	</xsl:template>

	<xsl:template match="course">
		<fo:block xsl:use-attribute-sets="docblock course-listing blockgroup">
			<xsl:attribute name="keep-together.within-page">always</xsl:attribute>
			<!--  
			<xsl:value-of select="
				concat(
				if (@offered='N') then '[' else ''
				, course_group
				, ' '
				, course_number/num_int
				, course_number/num_char
				, if (@offered='N') then ']' else ''
				, if (instructor_approval_required/text()='Y') then '*' else ''
				)"/>
			-->
			<xsl:apply-templates 
				mode="course_detail"
				select="title
				, course_number
				, @cat_num
				, faculty_text
				, credit 
				, course_level
				, prerequisites
				, description 
				, notes				
				"/> 	
		</fo:block>
		<fo:block>
			<fo:leader leader-pattern="rule"/>
		</fo:block>		
	</xsl:template>


	<xsl:template match="title" mode="course_detail">
		<fo:block xsl:use-attribute-sets="h4" >
			<xsl:apply-templates/>
		</fo:block> 
	</xsl:template>
	
	<xsl:template match="course_number" mode="course_detail">
		<fo:block xsl:use-attribute-sets="p">
			<xsl:apply-templates select="." mode="common"/>
			<xsl:text>. </xsl:text>
			<xsl:value-of select="title"/>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="@cat_num" mode="course_detail">
		<fo:block xsl:use-attribute-sets="p">
			<xsl:text>Catalog Number: </xsl:text>
			<xsl:value-of select="."/>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="faculty_text" mode="course_detail">
		<fo:block xsl:use-attribute-sets="p em"><xsl:apply-templates/></fo:block>
	</xsl:template>
	
	<xsl:template match="credit" mode="course_detail">
		<fo:block xsl:use-attribute-sets="p">
			<xsl:apply-templates/>
			<xsl:apply-templates 
				mode="course_detail"
				select="term, meeting_text, @offered"/>
		</fo:block>
	</xsl:template>
	<xsl:template match="@offered" mode="course_detail">
		<xsl:if test=".='N'">
			<xsl:text> (</xsl:text>
			<em>
				<xsl:text>next offered: </xsl:text>
				<xsl:value-of select="@next_year_offered"/> 
			</em>
			<xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="term" mode="course_detail">
		<xsl:text> (</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>). </xsl:text>
	</xsl:template>
	
	<xsl:template match="meeting_text[string-length(text()) gt 0]" mode="course_detail">
		<xsl:value-of select="translate(., ';.', '')"/>
		<xsl:text>.</xsl:text>
	</xsl:template>
	
	<xsl:template match="course_level" mode="course_detail">
		<fo:block xsl:use-attribute-sets="p">
			<xsl:apply-templates/>
			<xsl:text> / </xsl:text>
			<xsl:apply-templates select="../course_type"/>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="prerequisites" 
		mode="course_detail">
		<fo:block xsl:use-attribute-sets="p">
			<xsl:text>Prerequisites</xsl:text>
			<xsl:text>: </xsl:text>
			<xsl:choose>
				<xsl:when test="string-length(text()) gt 0">
					<xsl:apply-templates/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>NONE</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="instructor_approval_required[text() = 'Y']" mode="course_detail">
		<xsl:text>(INSTRUCTOR APPROVAL REQUIRED)</xsl:text>
	</xsl:template>
	
	<xsl:template match="description" mode="course_detail">
		<fo:block xsl:use-attribute-sets="p"><xsl:apply-templates/></fo:block>
	</xsl:template>
	
	<xsl:template match="notes[string-length(text()) gt 0]">
		<fo:block xsl:use-attribute-sets="p">
			<xsl:text>Note: </xsl:text>
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>


	
		
</xsl:stylesheet>
