<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"  
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
	version="2.0" 
	exclude-result-prefixes="xsl xs fo">

  <xsl:attribute-set name="fontstack">
    <xsl:attribute name="text-align">left</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
    <xsl:attribute name='font-family'>
    	'TradeGothic-CondEighteen', 'Trade Gothic Cond Eighteen', 'Trade Gothic Condensed Eighteen', 'Trade Gothic', 'TradeGothic', 'Trade-Gothic', 'ArialNarrow', 'Arial-Narrow', 'Arial Narrow', Arial, sans-serif
    </xsl:attribute>
    <xsl:attribute name="color">black</xsl:attribute>
  	<xsl:attribute name="font-weight">normal</xsl:attribute>
  	
  </xsl:attribute-set>
	<!-- page-master  -->
	<xsl:attribute-set name="page-master">
		<xsl:attribute name="page-height">11in</xsl:attribute>
		<xsl:attribute name="page-width">8.5in</xsl:attribute>
		<xsl:attribute name="margin">1.0in</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="region-body">
		<xsl:attribute name="margin-top">0.2in</xsl:attribute>
		<xsl:attribute name="margin-bottom">0.2in</xsl:attribute>
	</xsl:attribute-set>
<!--
page-height="11in" page-width="8.5in"
margin-top="1.0in" margin-bottom="1.0in" 
margin-left="1in" margin-right="1in"
-->
  <!-- surrounding the document block -->
  <xsl:attribute-set name="docblock" use-attribute-sets="fontstack">
  </xsl:attribute-set>

  <!-- surrounding the table block groups -->
  <xsl:attribute-set name="tablegroup">
    <xsl:attribute name="margin-bottom">0.5in</xsl:attribute>
  </xsl:attribute-set>

  <!-- surrounding the course detail block groups -->
  <xsl:attribute-set name="blockgroup">
    <xsl:attribute name="page-break-inside">avoid</xsl:attribute>
  </xsl:attribute-set>

  <!-- base style for headings -->
  <xsl:attribute-set name="headers">
    <xsl:attribute name="font-size">2em</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="padding">3em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="h1" use-attribute-sets="headers"/>
  
  <xsl:attribute-set name="h2" use-attribute-sets="headers">
    <!--<xsl:attribute name="break-before">page</xsl:attribute>-->
    <xsl:attribute name="padding">3</xsl:attribute>
    <xsl:attribute name="font-size">1.5em</xsl:attribute>
  </xsl:attribute-set>
	
	<xsl:attribute-set name="group-headers">
		<xsl:attribute name="padding-top">1.5em</xsl:attribute>
		<xsl:attribute name="padding-bottom">0.5em</xsl:attribute>
	</xsl:attribute-set>

  <xsl:attribute-set name="h3" use-attribute-sets="headers">
    <xsl:attribute name="padding">0.5em</xsl:attribute>
    <xsl:attribute name="space-before">2em</xsl:attribute>
    <xsl:attribute name="space-after">0em</xsl:attribute>
    <xsl:attribute name="font-size">1.6em</xsl:attribute>
    <xsl:attribute name="font-weight">200</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="h4" use-attribute-sets="headers">
    <xsl:attribute name="padding">0.75em</xsl:attribute>
    <xsl:attribute name="space-before">0em</xsl:attribute>
    <xsl:attribute name="space-after">0em</xsl:attribute>
    <xsl:attribute name="font-size">1.4em</xsl:attribute>
    <xsl:attribute name="font-weight">700</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="th" use-attribute-sets="h3">
    <xsl:attribute name="text-align">left</xsl:attribute>
    <xsl:attribute name="font-size">1.2em</xsl:attribute>
    <xsl:attribute name="font-weight">400</xsl:attribute>
    <xsl:attribute name="padding">1em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="p" >
    <xsl:attribute name="padding">0.5em</xsl:attribute>
    <xsl:attribute name="space-after">0em</xsl:attribute>
    <xsl:attribute name="font-size">1em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="callout">
    <xsl:attribute name="margin-left">2.125em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="em">
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="border_1">
    <xsl:attribute name="border-style">solid</xsl:attribute>
    <xsl:attribute name="border-width">thin</xsl:attribute>
    <xsl:attribute name="padding">0.2em</xsl:attribute>
  </xsl:attribute-set>

	<xsl:attribute-set name="dept-header">
		<xsl:attribute name="font-size">1.2em</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="dept-header-listing">
		<xsl:attribute name="border">1pt solid #708090</xsl:attribute>
		<xsl:attribute name="border-radius">20pt</xsl:attribute>
		<xsl:attribute name="font-size">1.2em</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="padding">1em</xsl:attribute>
		<xsl:attribute name="background-color">#F0FFF0</xsl:attribute>
		<xsl:attribute name="margin-bottom">2em</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="group-header">
		<xsl:attribute name="padding-top">2em</xsl:attribute>
		<xsl:attribute name="font-size">1.4em</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>	
	
	<xsl:attribute-set name="gh-border">
		<xsl:attribute name="padding">.75em</xsl:attribute>	
		<xsl:attribute name="background-color">#F0FFF0</xsl:attribute>
		<xsl:attribute name="border-bottom">1pt dotted #F0FFF0</xsl:attribute>
	</xsl:attribute-set>	
	
	<xsl:attribute-set name="course-listing">
		<xsl:attribute name="padding-top">1.2em</xsl:attribute>
		<xsl:attribute name="padding-bottom">.75em</xsl:attribute>			
		<!--<xsl:attribute name="border-bottom">1pt solid #F0FFF0</xsl:attribute>-->
	</xsl:attribute-set>	
	
	<xsl:attribute-set name="toc-padding">
		<xsl:attribute name="padding-top">4.em</xsl:attribute>
	</xsl:attribute-set>
	
	
	

</xsl:stylesheet>
