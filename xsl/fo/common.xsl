<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:import href="../common.xsl"/>
<xsl:import href="styles.xsl"/>	
	
  <xsl:variable name="dept_name"     
    select="/data/fas_courses/course[1]/department/dept_short_name"/>

  <xsl:template match="/">

    <fo:root>

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple" xsl:use-attribute-sets="page-master">  
		  <!-- Important: The margins you set for the region-body must be greater than or equal to the extents of the the region-before and region-after (and the region-start and region-end if you use them - FOP does not currently support them.) http://goo.gl/3xGkK -->
          <fo:region-body xsl:use-attribute-sets="region-body"/> 
          <fo:region-before extent="0.2in" /> 
          <fo:region-after extent="0.2in" /> 
        </fo:simple-page-master>
      </fo:layout-master-set>
      
      <fo:page-sequence 
        master-reference="simple">

        <!-- fo:static-content for header -->
        <fo:static-content flow-name="xsl-region-before">
            <fo:block font-size="8pt" text-align="end">
                <xsl:text>Harvard University Faculty of Arts and Sciences Course Catalog</xsl:text>
            </fo:block>
        </fo:static-content>

        <!-- fo:static-content for footer -->
        <fo:static-content flow-name="xsl-region-after">
            <fo:block font-size="8pt" text-align="right">
                <xsl:call-template name="page_number"/>
            </fo:block>
        </fo:static-content>

        <fo:flow flow-name="xsl-region-body">
          <fo:block>
            <xsl:apply-templates select="data/fas_courses"/>
          </fo:block>
          <fo:block id="terminator"/>
        </fo:flow>

      </fo:page-sequence>

    </fo:root>

  </xsl:template>

<!--
  <xsl:template match="course_number" mode="common">
    <xsl:value-of select="
    concat(
        if (../@offered='N') then '[' else ''
        , ../course_group
        , ' '
        , num_int
        , num_char
        , if (../@offered='N') then ']' else ''
        , if (../instructor_approval_required/text()='Y') then '*' else ''
        )"/>
    </xsl:template>
    -->

    <xsl:template name="page_number">
        <xsl:text>Page </xsl:text>
        <fo:page-number/>
        <xsl:text> of </xsl:text>
        <fo:page-number-citation ref-id="terminator"/>
    </xsl:template>

</xsl:stylesheet>
