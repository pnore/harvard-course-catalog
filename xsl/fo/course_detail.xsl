<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
    <xsl:import href="common.xsl"/>
    <xsl:import href="styles.xsl"/>    
    <xsl:param name="cat_num"/>
    
    <xsl:template match="fas_courses">
        <fo:block xsl:use-attribute-sets="docblock">
            <xsl:apply-templates select="course[@cat_num=$cat_num]"/>
        </fo:block>
        
    </xsl:template>
    
    <xsl:template match="course">
        <xsl:value-of select="
            concat(
            if (@offered='N') then '[' else ''
            , course_group
            , ' '
            , course_number/num_int
            , course_number/num_char
            , if (@offered='N') then ']' else ''
            , if (instructor_approval_required/text()='Y') then '*' else ''
            )"/>

        <xsl:apply-templates 
            mode="course_detail"
            select="title
            , course_number
            , @cat_num
            , faculty_text
            , credit 
            , course_level
            , prerequisites
            "/>        
        
        <xsl:apply-templates select="description, notes"/>
        
    </xsl:template> 
    
    
    <xsl:template match="title" mode="course_detail">
        <fo:block xsl:use-attribute-sets="h4">
            <xsl:apply-templates/>
        </fo:block> 
    </xsl:template>
    
    <xsl:template match="course_number" mode="course_detail">
        <fo:block xsl:use-attribute-sets="p">
            <xsl:apply-templates select="." mode="common"/>
            <xsl:text>. </xsl:text>
            <xsl:value-of select="title"/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="@cat_num" mode="course_detail">
        <fo:block xsl:use-attribute-sets="p">
            <xsl:text>Catalog Number: </xsl:text>
            <xsl:value-of select="."/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="faculty_text" mode="course_detail">
        <fo:block xsl:use-attribute-sets="p em"><xsl:apply-templates/></fo:block>
    </xsl:template>
    
    <xsl:template match="credit" mode="course_detail">
        <fo:block xsl:use-attribute-sets="p">
            <xsl:apply-templates/>
            <xsl:apply-templates 
                mode="course_detail"
                select="term, meeting_text, @offered"/>
        </fo:block>
    </xsl:template>
    <xsl:template match="@offered" mode="course_detail">
        <xsl:if test=".='N'">
            <xsl:text> (</xsl:text>
            <em>
                <xsl:text>next offered: </xsl:text>
                <xsl:value-of select="@next_year_offered"/> 
            </em>
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="term" mode="course_detail">
        <xsl:text> (</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>). </xsl:text>
    </xsl:template>
    
    <xsl:template match="meeting_text[string-length(text()) gt 0]" mode="course_detail">
        <xsl:value-of select="translate(., ';.', '')"/>
        <xsl:text>.</xsl:text>
    </xsl:template>
    
    <xsl:template match="course_level" mode="course_detail">
        <fo:block xsl:use-attribute-sets="p">
            <xsl:apply-templates/>
            <xsl:text> / </xsl:text>
            <xsl:apply-templates select="../course_type"/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="prerequisites" 
        mode="course_detail">
        <fo:block xsl:use-attribute-sets="p">
            <xsl:text>Prerequisites</xsl:text>
            <xsl:text>: </xsl:text>
            <xsl:choose>
                <xsl:when test="string-length(text()) gt 0">
                    <xsl:apply-templates/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>NONE</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="instructor_approval_required[text() = 'Y']" mode="course_detail">
        <xsl:text>(INSTRUCTOR APPROVAL REQUIRED)</xsl:text>
    </xsl:template>
    
    <xsl:template match="description" mode="course_detail">
        <fo:block xsl:use-attribute-sets="p"><xsl:apply-templates/></fo:block>
    </xsl:template>
    
    <xsl:template match="notes[string-length(text()) gt 0]">
        <fo:block xsl:use-attribute-sets="p">
            <xsl:text>Note: </xsl:text>
            <xsl:apply-templates/></fo:block>
    </xsl:template>
    
    
    
</xsl:stylesheet>
