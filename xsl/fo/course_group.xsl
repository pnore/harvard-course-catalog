<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:pmn="http://www.myernore.com" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" exclude-result-prefixes="pmn xsl xs fo">
  <xsl:import href="common.xsl"/>
  <xsl:import href="styles.xsl"/>
  <xsl:param name="dept"/>  
  <!-- slct 1st crs of ech crs grp in $dept -->
  <xsl:variable name="depts" as="element()*">
    <xsl:for-each-group 
        select="/data/fas_courses/course" group-by="course_group">
      <xsl:sequence select="current-group()[1]"/>
    </xsl:for-each-group>
  </xsl:variable>
  <!-- /slct 1st crs of ech crs grp in $dept -->
  <!-- slct crses in $dept -->
  <xsl:variable 
    name="courses" 
    select="/data/fas_courses/course"/>
  <xsl:variable name="page_title">
    <xsl:text>Department Courses</xsl:text>
    <!--<xsl:value-of select="$courses[1]/title"/>-->
  </xsl:variable>
  <xsl:variable name="separator_non_table">
    <fo:block text-align-last="justify">
    <fo:leader leader-pattern="dots"/>
    </fo:block> 
  </xsl:variable>
  <xsl:variable name="dept_course_table_headings">
    <fo:table-row xsl:use-attribute-sets="th border_1">
      <fo:table-cell>
        <fo:block>
          <xsl:text>Page</xsl:text>
        </fo:block>
      </fo:table-cell>
      <fo:table-cell>
        <fo:block>
          <xsl:text>Course Number</xsl:text>
        </fo:block>
      </fo:table-cell>
      <fo:table-cell>
        <fo:block>
          <xsl:text>Term</xsl:text>
        </fo:block>
      </fo:table-cell>
      <fo:table-cell>
        <fo:block>
          <xsl:text>Course Title</xsl:text>
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </xsl:variable>
  <xsl:template match="fas_courses">
    <!-- docblock surrounds whole document -->
    <fo:block xsl:use-attribute-sets="docblock">
        <!-- dept pdf headr -->    
    	<!-- TODO: MAKE THIS WORK FOR MULTIPLE DEPARTMENTS 
    	<fo:block xsl:use-attribute-sets="h1">
      
           <xsl:value-of 
            select="/data/fas_courses/course[1]/department/dept_long_name"/>
        </fo:block>
         -->    

    	<!-- gt 1 course_group --><!-- course group toc -->
     <!--
    	<xsl:if test="count($depts) gt 1">
        <fo:block id="top_course_groups" xsl:use-attribute-sets="tablegroup">
          <fo:block xsl:use-attribute-sets="h2">
              <xsl:text>Course Group(s):</xsl:text>
          </fo:block>
          <fo:table>
            <fo:table-column column-width="7in"/>
            <fo:table-body xsl:use-attribute-sets="p">
              <xsl:apply-templates select="$depts/course_group" mode="toc"/>
            </fo:table-body>
          </fo:table>
        </fo:block>
      </xsl:if>
		-->

    	<fo:block>
    		<xsl:attribute name="break-after">page</xsl:attribute>
	    	<xsl:for-each-group select="$courses" group-by="course_group/@code">
		        
	    		<xsl:if test="count($depts) gt 1">
		          <fo:block id="{generate-id(course_group)}"
		            xsl:use-attribute-sets="tablegroup">
		          	<fo:block xsl:use-attribute-sets="h2 group-headers">
		              <!--<xsl:text>Course Group: </xsl:text>-->
		              <xsl:value-of select="course_group"/>
		            </fo:block>
		          </fo:block>
		        </xsl:if>
		
		          <!-- course group table -->
		          <fo:table>
		            <fo:table-column column-width="0.5in"/>
		            <fo:table-column column-width="1.75in"/>
		            <fo:table-column column-width="1in"/>
		            <fo:table-column column-width="3.5in"/>
		            <fo:table-body>
		              <xsl:copy-of select="$dept_course_table_headings"/>
		              <xsl:apply-templates select="current-group()" mode="table-row">
		                <xsl:sort select="course_number/num_int" data-type="number"/>
		                <xsl:sort select="course_number/num_char"/>
		              </xsl:apply-templates>
		            </fo:table-body>
		          </fo:table>
		          <!-- /course group table -->
	
	        </xsl:for-each-group>
    	</fo:block>

        <!-- /course groups -->

        <!-- course details -->
          <xsl:apply-templates select="$courses" mode="course_detail">
            <xsl:sort select="course_number/num_int" data-type="number"/>
            <xsl:sort select="course_number/num_char"/>
          </xsl:apply-templates>
        <!-- course details -->

    </fo:block><!-- /docblock -->
  </xsl:template>
  <xsl:template match="course" mode="course_detail">

    <fo:block xsl:use-attribute-sets="blockgroup">
    <xsl:attribute name="break-after">page</xsl:attribute>

      <!-- course detail heading -->
      <fo:block id="{generate-id()}" xsl:use-attribute-sets="h3">
        <!--<xsl:text>Course Detail: </xsl:text>-->
        <xsl:apply-templates select="course_number"/>
      </fo:block>
      <!-- /course group heading -->

        <!-- course detail body -->
        <xsl:apply-templates 
        mode="course_detail"
        select="title
            , course_number
            , @cat_num
            , faculty_text
            , credit 
            , course_level
            , prerequisites
            "/>
        <xsl:apply-templates 
            mode="course_detail_callout"
            select="description, notes"/>
        <!-- /course detail body -->
    </fo:block>
  </xsl:template>
  <xsl:template match="*" mode="course_detail_callout">
    <!-- TODO: handle regular course details like this 
         that wraps a pass-through apply-templates in 
         a block that applies the default styling -->
    <fo:block xsl:use-attribute-sets="callout">
       <xsl:apply-templates select="." mode="course_detail"/> 
    </fo:block>
  </xsl:template>
  <xsl:template match="course" mode="table-row">
    <fo:table-row>
      <fo:table-cell xsl:use-attribute-sets="border_1">
        <fo:block>

          <fo:basic-link>
            <xsl:attribute name="internal-destination">
              <xsl:value-of select="generate-id()"/>
            </xsl:attribute>
            p.<fo:page-number-citation ref-id="{generate-id()}" as="xs:text"/>
          </fo:basic-link>

        </fo:block>
      </fo:table-cell>
      <fo:table-cell xsl:use-attribute-sets="border_1">
        <fo:block>

          <fo:basic-link>
            <xsl:attribute name="internal-destination">
              <xsl:value-of select="generate-id()"/>
            </xsl:attribute>
            <xsl:apply-templates select="course_number"/>
          </fo:basic-link>

        </fo:block>
      </fo:table-cell>
      <fo:table-cell xsl:use-attribute-sets="border_1">
        <fo:block>
          <xsl:apply-templates select="term"/>
        </fo:block>
      </fo:table-cell>
      <fo:table-cell xsl:use-attribute-sets="border_1">
        <fo:block>
          <xsl:apply-templates select="title"/>
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </xsl:template>
  <xsl:template match="course_number">
    <xsl:apply-templates mode="common" select="."/>
  </xsl:template>
	
	
	
  <xsl:template match="course_group" mode="toc">
    <fo:table-row>
      <fo:table-cell>
        <fo:block  xsl:use-attribute-sets="th">
          <fo:basic-link>
            <xsl:attribute name="internal-destination">
              <xsl:value-of select="generate-id()"/>
            </xsl:attribute>
            <xsl:apply-templates select="."/>
          </fo:basic-link>
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </xsl:template>



  <xsl:template match="title" mode="course_detail">
      <fo:block xsl:use-attribute-sets="h4">
      <xsl:apply-templates/>
      </fo:block> 
  </xsl:template>

  <xsl:template match="course_number" mode="course_detail">
    <fo:block xsl:use-attribute-sets="p">
        <xsl:apply-templates select="." mode="common"/>
        <xsl:text>. </xsl:text>
        <xsl:value-of select="../title"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="@cat_num" mode="course_detail">
    <fo:block xsl:use-attribute-sets="p">
        <xsl:text>Catalog Number: </xsl:text>
        <xsl:value-of select="."/>
    </fo:block>
  </xsl:template>

  <xsl:template match="faculty_text" mode="course_detail">
    <fo:block xsl:use-attribute-sets="p em"><xsl:apply-templates/></fo:block>
  </xsl:template>

  <xsl:template match="credit" mode="course_detail">
    <fo:block xsl:use-attribute-sets="p">
    <xsl:apply-templates/>
    <xsl:apply-templates 
        mode="course_detail"
        select="../term, ../meeting_text, ../@offered"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="@offered" mode="course_detail">
      <xsl:if test=".='N'">
        <xsl:text> (</xsl:text>
         <em>
           <xsl:text>next offered: </xsl:text>
           <xsl:value-of select="../@next_year_offered"/> 
         </em>
        <xsl:text>)</xsl:text>
      </xsl:if>
  </xsl:template>

  <xsl:template match="term" mode="course_detail">
    <xsl:text> (</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>). </xsl:text>
  </xsl:template>

  <xsl:template match="meeting_text[string-length(text()) gt 0]" mode="course_detail">
    <xsl:value-of select="translate(., ';.', '')"/>
    <xsl:text>.</xsl:text>
  </xsl:template>

  <xsl:template match="course_level" mode="course_detail">
    <fo:block xsl:use-attribute-sets="p">
    <xsl:apply-templates/>
    <xsl:text> / </xsl:text>
    <xsl:apply-templates select="../course_type"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="prerequisites" 
    mode="course_detail">
    <fo:block xsl:use-attribute-sets="p">
        <xsl:text>Prerequisites</xsl:text>
        <xsl:text>: </xsl:text>
        <xsl:choose>
            <xsl:when test="string-length(text()) gt 0">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>NONE</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </fo:block>
  </xsl:template>

  <xsl:template match="instructor_approval_required[text() = 'Y']" mode="course_detail">
    <xsl:text>(INSTRUCTOR APPROVAL REQUIRED)</xsl:text>
  </xsl:template>

  <xsl:template match="description" mode="course_detail">
    <fo:block xsl:use-attribute-sets="p"><xsl:apply-templates/></fo:block>
  </xsl:template>

  <xsl:template match="notes[string-length(text()) gt 0]" mode="course_detail">
    <fo:block xsl:use-attribute-sets="p">
        <em><xsl:text>Note: </xsl:text></em>
    <xsl:apply-templates/></fo:block>
  </xsl:template>

</xsl:stylesheet>
