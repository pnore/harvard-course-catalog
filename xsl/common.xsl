<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:navy="http://www.made_up_url_for_team_navy.com" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    version="2.0" 
    exclude-result-prefixes="navy xsl xs">

  <xsl:param name="url"/>
  <xsl:param name="medium"/>
  <xsl:param name="view"/>
  <xsl:param name="querystring"/>
  <xsl:param name="baselink"/>
  <!--
    Outputs course_number formatted according to conventions described in 
    page 7 of Assignment 4. 
    http://isites.harvard.edu/fs/docs/icb.topic1152394.files/CSCIE153_Assignment2_Fall2012.pdf

    Copied here:
    
+   Course Abbreviations and numbers:  Courses are routinely referred to by
"Course Group Number Character" (which corresponds in the XML to course_group,
course_number/num_int course_number/num_char).   Examples are "Chemistry 17"
and "Chemistry 91r" ("91" is the "num_int", "r" is the "num_char").    The
number value correlates to the level of the course, so it is common to sort
lists of courses by their "num_int" (secondarily by their "num_char").  

+   [Bracketed Courses]:  The common practice in FAS is to represent courses
that are not offered in the current academic year by putting brackets around
the title.  Students can still see that the course exists and when it will be
offered again, but the brackets are a flag to indicate that the course is not
available in the current academic year.  For example, see "[Chemistry 60.
Foundations of Physical Chemistry]".  

+   *Asterisked Courses:  Some courses require an instructor's approval to
enroll (in the data, the "instructor_approval_required" element).  These
courses are denoted by asterisks.  For example, see "*Chemistry 91r.
Introduction to Research".  

+   Faculty Text and Meeting Text:  For faculty and meeting listings, there are
two methods this is represented in the data.  For display purposes, the
"faculty_text" and "meeting_text" elements should be used - this contains a
human-readable summary of the instructors and meeting times.  The more
data-centric representation of this information (see "faculty_list" and
"schedule" elements) will be useful in later assignments for searching and
filtering.  For this assignment, use the elements "faculty_text" and
"meeting_text" to display information in the course detail page.

  -->
  <xsl:template match="course_number" mode="common">
    <xsl:value-of select="
    concat(
        if (../@offered='N') then '[' else ''
        , ../course_group
        , ' '
        , num_int
        , num_char
        , if (../@offered='N') then ']' else ''
        , if (../instructor_approval_required/text()='Y') then '*' else ''
        )"/>
    </xsl:template>

</xsl:stylesheet>
