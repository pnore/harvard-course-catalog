<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:h="http://apache.org/cocoon/request/2.0" 
    exclude-result-prefixes="h xs xi"
    version="2.0">
  <xsl:key 
    name="bldg" 
    match="/data/raw/pre-analysis/buildings/building/" 
    use="name"/> 
  <xsl:key 
    name="building_name_to_filtered_cat_num" 
    match="/data/fas_courses/course/@cat_num"
    use="../meeting_locations/location/@building"/> 

  <xsl:template match="building">
  <building>
    <xsl:apply-templates select="name|lat|lng"/>
  <courses>
   <filtered>
    <xsl:apply-templates select="key('building_name_to_filtered_cat_num',
        name)/.."/>
   </filtered>
   <all>
    <xsl:apply-templates select="course"/>
   </all>
   </courses>
  </building>
  </xsl:template>

  <xsl:template match="name|lat|lng">
   <xsl:element name="{local-name(.)}">
    <xsl:apply-templates/>
   </xsl:element>
  </xsl:template>

  <xsl:template match="course">
   <course><xsl:value-of select="@cat_num"/>
   </course>

  </xsl:template>

  <xsl:template match="/">
    <buildings>
    <xsl:copy>
     <xsl:apply-templates
     select="data/raw/pre_analysis/buildings/building"/>
     </xsl:copy>
    </buildings>
  </xsl:template>

</xsl:stylesheet>
