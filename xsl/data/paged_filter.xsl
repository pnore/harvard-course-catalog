<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:h="http://apache.org/cocoon/request/2.0" 
    xmlns:xi="http://www.w3.org/2001/XInclude"
    version="2.0">
  <xsl:import href="../common.xsl"/>

<!-- pagination -->
<xsl:param name="count" />
<xsl:param name="blocknr" />
<!-- Department: department/@code -->
<xsl:param name="department" select="'All'"/>

<!-- Course Group: "course_group/@code" -->
<xsl:param name="course_group" select="'All'"/>

<!-- Offered: "@offered" -->
<xsl:param name="offered" select="'All'"/>

<!-- Level: "course_level/@code" -->
<xsl:param name="level" select="'All'"/>

<!-- Description: case-insensitive search for string within "description" -->
<xsl:param name="description" select="'All'"/>

<!-- Term Pattern: "term/@term_pattern_code" -->
<xsl:param name="term" select="'All'"/>

<!-- Year - either course/@acad_year|course/@next_year_offered -->
<xsl:param name="year" select="'All'"/>

<!-- cat_num-->
<xsl:param name="cat_num" select="'All'"/>

  <xsl:template match="/">
    <data>
    <fas_courses>
      <xsl:apply-templates select="data/fas_courses/course"/>
    </fas_courses>
    <xsl:copy-of select="data/raw"/>
    </data>
  </xsl:template>

  <xsl:template match="course">
    <course>
     <xsl:attribute name="blocknr">
       <xsl:value-of select="ceiling(position() div number($count))"/>
     </xsl:attribute>
       <xsl:attribute name="lastblock">
       <xsl:value-of select="
         if (position() + number($count) gt last())
         then 1
         else 0"/>
       </xsl:attribute>
       <xsl:copy-of select="@*|*"/>
    </course>
  </xsl:template>

  <xsl:template match="text()"/>
</xsl:stylesheet>
