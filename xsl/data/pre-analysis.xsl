<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:h="http://apache.org/cocoon/request/2.0" 
    xmlns:xi="http://www.w3.org/2001/XInclude"
    version="2.0">

  <xsl:key name="lat" match="/data/buildings/building/latitude" use="../name"/> 
  <xsl:key name="lng" match="/data/buildings/building/longitude" use="../name"/> 
  <xsl:template match="/">
    <data>
      <xsl:copy-of select="data/fas_courses"/>
      <xsl:copy-of select="data/request"/>
      <pre_analysis>
        <buildings>
            <xsl:for-each-group select="/data/fas_courses/course" 
            group-by="meeting_locations/location/@building">
                <building>
                <name><xsl:value-of select="current-grouping-key()"/></name>
                <lat><xsl:value-of 
                    select="key('lat',current-grouping-key())"/>
                </lat>
                <lng><xsl:value-of 
                    select="key('lng',current-grouping-key())"/>
                </lng>
                <xsl:for-each select="current-group()/@cat_num">
                    <course cat_num="{current()}">
                    <!--
                    <xsl:attribute name="shown">
                    <xsl:value-of select="count(/data/fas_courses/block/course/@cat_num[.=current()])"/>
                    </xsl:attribute>
                    -->
                    </course>
                </xsl:for-each>
                </building>
            </xsl:for-each-group>
        </buildings>
      </pre_analysis>
    </data>
  </xsl:template>

</xsl:stylesheet>
