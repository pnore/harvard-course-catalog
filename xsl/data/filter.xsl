<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:h="http://apache.org/cocoon/request/2.0" 
    version="2.0">
  <xsl:import href="../common.xsl"/>

<!-- Department: department/@code -->
<xsl:param name="department" select="'All'"/>

<!-- Course Group: "course_group/@code" -->
<xsl:param name="course_group" select="'All'"/>

<!-- Offered: "@offered" -->
<xsl:param name="offered" select="'All'"/>

<!-- Level: "course_level/@code" -->
<xsl:param name="level" select="'All'"/>

<!-- Description: case-insensitive search for string within "description" -->
<xsl:param name="description" select="'All'"/>

<!-- Term Pattern: "term/@term_pattern_code" -->
<xsl:param name="term" select="'All'"/>

<!-- Year - either course/@acad_year|course/@next_year_offered -->
<xsl:param name="year" select="'All'"/>

<!-- cat_num-->
<xsl:param name="cat_num" select="'All'"/>

<!-- pagination -->
<xsl:param name="count" />
<xsl:param name="blocknr" />

  <xsl:template match="/">
    <data>
    <fas_courses>
      <xsl:apply-templates select="data/fas_courses/course">
        <xsl:sort select="department/dept_short_name"/>
        <xsl:sort select="course_group"/>
        <xsl:sort select="course_number/num_int" data-type="number"/>
        <xsl:sort select="course_number/num_char"/>
      </xsl:apply-templates>
    </fas_courses>
    <raw>
        <xsl:copy-of select="data/@*|data/*"/>
    </raw>
    <post-analysis>
<!-- this is where I left off 3:35 am i was about to implement max block nr -->
    </post-analysis>
    </data>
  </xsl:template>

<!--
  <xsl:template match="course">remove</xsl:template>
  -->

  <!--
  multiple match for department, course_group, term
  -->
  <xsl:template match="course[
     (
        $department = '' 
        or $department = 'All' 
        or department/@code = 
            /data/request/h:request/h:requestParameters/
            h:parameter[@name='department']/h:value/text()
     )
     and
     (
        $course_group = '' 
        or $course_group = 'All' 
        or course_group/@code = 
            /data/request/h:request/h:requestParameters/
            h:parameter[@name='course_group']/h:value/text())
     and
     (
        $offered = '' 
        or $offered = 'All' 
        or @offered = $offered)
     and
     (
        $year = '' 
        or $year = 'All' 
        or (@acad_year = $year and @offered='Y')
        or (@next_year_offered = $year and @offered='N'))
     and
     (
        $level = '' 
        or $level = 'All' 
        or course_level/@code = $level)
     and
     (
        $description = '' 
        or $description = 'All' 
        or matches(
            description,
            /data/request/h:request/h:requestParameters
            /h:parameter[@name='description']/h:value[1]/text())
     )
     and
     (
        $term = '' or 
        $term = 'All' 
        or term/@term_pattern_code = 
            /data/request/h:request/h:requestParameters/
            h:parameter[@name='term']/h:value/text())
     and
     (
        $cat_num = '' or 
        $cat_num = 'All' 
        or @cat_num = 
            /data/request/h:request/h:requestParameters/
            h:parameter[@name='cat_num']/h:value/text())
    ]">
    <!--<xsl:copy-of select="."/>-->
    <course>
    <!--
       <xsl:attribute name="blocknr">
       <xsl:value-of select="ceiling(position() div number($count))"/>
       </xsl:attribute>
       -->
       <!--
       <xsl:attribute name="lastblock">
       <xsl:value-of select="
         if (position() + number($count) gt last())
         then 1
         else 0"/>
       </xsl:attribute>
       -->
       <xsl:copy-of select="@*|*"/>
    </course>
  </xsl:template>

  <xsl:template match="text()"/>
</xsl:stylesheet>
