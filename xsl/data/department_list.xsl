<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:param name="department" select="'All'"/>
<!-- 
filename: department_list.xsl
output rnc: 
start =
    element departments {
        element department {
        attribute code { text },
        attribute name { text }
        }+
    }
-->
  <xsl:key name="kDept" match="course/department" use="@code"/>
  <xsl:template match="/">
    <departments>
      <xsl:for-each-group select="fas_courses/course/department" group-by="@code">
        <xsl:sort select="dept_short_name"/>
        <department>
            <xsl:attribute name="code">
                <xsl:value-of select="current-grouping-key()"/>
            </xsl:attribute>
            <xsl:attribute name="name">
                <xsl:value-of select="current-group()[1]/dept_short_name"/>
            </xsl:attribute>
        </department>
      </xsl:for-each-group>
    </departments>
  </xsl:template>
</xsl:stylesheet>

