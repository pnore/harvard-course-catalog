<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
    xmlns:h="http://apache.org/cocoon/request/2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">
    <xsl:import href="common.xsl" />
    <xsl:import href="../common.xsl" />
    <!--<xsl:strip-space elements="*" />-->
    <xsl:param name="dept"/>
    <xsl:param name="cat_num"/>
    <xsl:variable name="courses" select="/fas_courses/course[@cat_num=$cat_num]"/>
    
    <xsl:variable name="page_title" >
    Course Detail: 
    <!--    <xsl:value-of select="$courses[1]/department/dept_short_name"/>-->
    </xsl:variable>

    <xsl:template name="content">
       <!--<xsl:template match="fas_courses">-->
        <xsl:apply-templates select="/data/fas_courses/course"/>
    </xsl:template>
    
    <xsl:template match="course">
        <xsl:apply-templates 
            select="
            course_number
            , title
            , @cat_num
            , faculty_text
            , credit 
            , course_level
            , prerequisites
            , description 
            , notes
            "/>
    </xsl:template>
    
    <xsl:template match="title">
        <h2>
            <xsl:apply-templates/>
            <xsl:text>&#160;</xsl:text>
            <xsl:call-template name="pdf-link"/>
        </h2> 
        
    </xsl:template>
    
    <xsl:template match="course_number">
        <hr/><h1 id="{concat('course-',../@cat_num)}">
          <!--<a href="{concat('#course-',../@cat_num)}">-->
        	<a href="{concat('filter?cat_num=',../@cat_num)}">
            <xsl:apply-templates select="." mode="common"/>
            <xsl:text>. </xsl:text>
            <xsl:value-of select="../title"/>
          </a>
        </h1>
    </xsl:template>
    
    <xsl:template match="@cat_num">
        <p>
            <xsl:text>Catalog Number: </xsl:text>
            <xsl:value-of select="."/>
        </p>
    </xsl:template>
    
    <xsl:template match="faculty_text">
        <p><em><xsl:apply-templates/></em></p>
    </xsl:template>
    
    <xsl:template match="credit">
        <p>
            <xsl:apply-templates/>
            <xsl:apply-templates select="../term, ../meeting_text, ../@offered"/>
        </p>
    </xsl:template>
    
    <xsl:template match="@offered">
        <xsl:if test=".='N'">
            <xsl:text> (</xsl:text>
            <em>
                <xsl:text>next offered: </xsl:text>
                <xsl:value-of select="../@next_year_offered"/> 
            </em>
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="term">
        <xsl:text> (</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>). </xsl:text>
    </xsl:template>
    
    <xsl:template match="meeting_text">
        <xsl:value-of select="translate(., ';.', '')"/>
        <xsl:text>.</xsl:text>
    </xsl:template>
    
    <xsl:template match="course_level">
        <p>
            <xsl:apply-templates/>
            <xsl:text> / </xsl:text>
            <xsl:apply-templates select="../course_type"/>
        </p>
    </xsl:template>
    
    <xsl:template match="prerequisites[string-length(text()) gt 0]">
        <p>
            <strong>
                <xsl:text>Prerequisites</xsl:text>
                <xsl:text>: </xsl:text>
            </strong>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    <xsl:template match="instructor_approval_required[text() = 'Y']">
        <xsl:text>(INSTRUCTOR APPROVAL REQUIRED)</xsl:text>
    </xsl:template>
    
    <xsl:template match="description">
        <p class="course description"><xsl:apply-templates/></p>
    </xsl:template>
    
    <xsl:template match="notes[string-length(text()) gt 0]">
        <p class="course note">
            <em><xsl:text>Note: </xsl:text></em>
            <xsl:apply-templates/></p>
    </xsl:template>
    
    <xsl:template name="pdf-link">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="
                    concat($baselink,'fo',
                    '/course_detail/filter?',$querystring,'&amp;','cat_num=',
                    ../@cat_num 
                    )"/>
            </xsl:attribute>
            <img src="{$baselink}img/pdf-icon.png" alt="remove"/>            
        </a>        
    </xsl:template>
    
</xsl:stylesheet>
