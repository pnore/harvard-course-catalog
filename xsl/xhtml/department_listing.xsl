<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
  xmlns:h="http://apache.org/cocoon/request/2.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
  <xsl:import href="common.xsl" />
  <xsl:variable name="title">Course Catalog - Department Listing</xsl:variable>
  <xsl:param name="href" select="concat('course_groups','/','CG-')"></xsl:param>
  <xsl:param name="url"/>
  <xsl:param name="querystring"/>
  <xsl:param name="baselink"/>
  <xsl:param name="medium"/>
  <xsl:param name="view"/>
  <!--<xsl:variable name="relpath" select="concat($baselink,$medium,'/',$view,'/')"/>-->
  <xsl:template name="content">
      <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="fas_courses">
     <h1>Harvard University 2012-13 Course Catalog - Department Listing</h1> 
     <ol>
       <xsl:for-each-group select="course" group-by="department/@code">
         <xsl:sort select="department/@code"/>
         <li><strong>
           <xsl:value-of select="department/dept_short_name"/>
         </strong> 
         <ol>
           <xsl:for-each-group 
             select="current-group()" group-by="course_group/@code">
             <li>
               <!--<a >
                  <xsl:attribute name="href">
                     <xsl:value-of select="
                     concat(
                        $baselink,
                        $medium,
                        '/course_group/filter?',
                        'course_group=',
                        current-grouping-key())"/>
                  </xsl:attribute>
               <xsl:value-of select="./course_group"/>
               </a>-->
               <a >
                 <xsl:attribute name="href">
                   <xsl:value-of select="
                     concat(
                     $baselink,
                     $medium,
                     '/course_group/',
                     @blocknr,
                     '/?',
                     'course_group=',
                     current-grouping-key())"/>
                 </xsl:attribute>
                 <xsl:value-of select="./course_group"/>
               </a>
               <!--<xsl:value-of select="./@blocknr"/>-->
             </li>
           </xsl:for-each-group>
         </ol>
         </li>
       </xsl:for-each-group>
     </ol>
  </xsl:template>    
</xsl:stylesheet>
