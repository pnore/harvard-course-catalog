<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
  xmlns:h="http://apache.org/cocoon/request/2.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

  <!-- extract my query string param sequence -->
  <xsl:variable name="myqsparamseq" as="item()*">
    <!-- extract a sequence of parameter values in the query string
    separated by ampersands -->
    <xsl:sequence select="for $pv in tokenize($querystring,'&amp;') return $pv"/>
  </xsl:variable>
  <xsl:template name="sidesearch">
    <form method="get" action="{$baselink}course/filter" id="search-form">

    <h4>Search</h4>
    <xsl:call-template name="search_calendar_period_checkboxes"/>
    <!--
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Calendar Period
    </h3>
    <select name="fq_coordinated_semester_yrValue">
    <option value="">Any </option>
    <option value="coordinated_semester_yr:&quot;Jun to Aug 2012 (Summer Term)&quot;">Jun to Aug 2012 (Summer Term)</option>
    <option value="coordinated_semester_yr:&quot;Sep to Dec 2012 (Fall Term)&quot;">Sep to Dec 2012 (Fall Term)</option>
    <option value="coordinated_semester_yr:&quot;Jan 2013 (Winter Session)&quot;">Jan 2013 (Winter Session)</option>
    <option value="coordinated_semester_yr:&quot;Jan to May 2013 (Spring Term)&quot;">Jan to May 2013 (Spring Term)</option>
    </select>
    <input type="hidden" name="inputField" value="fq_coordinated_semester_yr"/>
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Calendar Period
    </h3>
    <select name="fq_coordinated_semester_yrValue">
    <option value="">Any </option>
    <option value="coordinated_semester_yr:&quot;Jun to Aug 2012 (Summer Term)&quot;">Jun to Aug 2012 (Summer Term)</option>
    <option value="coordinated_semester_yr:&quot;Sep to Dec 2012 (Fall Term)&quot;">Sep to Dec 2012 (Fall Term)</option>
    <option value="coordinated_semester_yr:&quot;Jan 2013 (Winter Session)&quot;">Jan 2013 (Winter Session)</option>
    <option value="coordinated_semester_yr:&quot;Jan to May 2013 (Spring Term)&quot;">Jan to May 2013 (Spring Term)</option>
    </select>
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Description
    </h3>
    <input type="hidden" name="inputField" value="fq_coordinated_semester_yr"/>
    <input type="text" id="search-input" name="P" size="32" maxlength="256" value="" />
    <input type="image" align="bottom" src="{$baselink}img/btn_go.gif" id="search-submit"/>
    -->
    </form>
  </xsl:template>
  <xsl:template name="search_calendar_period_links">
    <xsl:variable name="myqs">
      <xsl:value-of select="replace($querystring,'can_num=\d*','')"/>
    </xsl:variable>
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Select Calendar Period
    </h3>
    <div id="search_calendar_period">
    <xsl:for-each-group 
      select="/data/raw/fas_courses/course/term"  
      group-by="@term_pattern_code">
      <xsl:sort select="current-grouping-key()" data-type="number"/>
      <!-- query string parameter to potentially add to url -->
      <xsl:variable name="qsparam">
        <!-- is the term=CHEM -->
        <xsl:value-of select="concat('term=', current-grouping-key())"/>
      </xsl:variable>
      <!-- what to put between the anchor tags: <a>link_body</a> -->
      <xsl:variable name="link_body">
        <xsl:value-of select="current-group()[1]"/>
      </xsl:variable>
      <xsl:if test="string-length(current-grouping-key()) > 0">
        <xsl:choose>
          <!-- when current query string parameter 
          <xsl:when test="$qsparam = $myqsparamseq">
            <xsl:value-of select="$link_body"/>
          </xsl:when>
            
        <input onclick="this.form.submit()" type="checkbox" name="term">
            <xsl:attribute name="value">
                <xsl:value-of select="current-grouping-key()"/> 
            </xsl:attribute>
            <xsl:attribute name="current_grp_term">
                <xsl:value-of select="current-group()[1]/@term_pattern_code"/>
            </xsl:attribute>
            <xsl:attribute name="url_with_current_val">
                <xsl:value-of select="
                /data/raw/request/h:request/h:requestParameters/h:parameter
                    [@name = 'term']
                    /h:value/text()
                "/>
            </xsl:attribute>
            <xsl:attribute name="local_name">
                <xsl:value-of select="
                    local-name(current-group()[1])
                "/>
            </xsl:attribute>
            <xsl:attribute name="checked">
                <xsl:value-of select="if 
                  (current-group()[1]/@term_pattern_code = 
                  /data/raw/request/h:request/h:requestParameters/h:parameter
                    [@name = local-name(current-group()[1])]
                    /h:value/text())
                  then 'true' else ''"/>
            </xsl:attribute>
           <xsl:value-of select="current-group()[1]"/>
        </input>
        <xsl:if test="position() != last()"><br/></xsl:if>

    </xsl:for-each-group>
    </div><!-- /search_calendar_period_links -->
  </xsl:template>
  <xsl:template name="search_calendar_period_checkboxes">
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Calendar Period
    </h3>
    <xsl:for-each-group 
      select="/data/raw/fas_courses/course/term"  
      group-by="@term_pattern_code">
        <input onclick="this.form.submit()" type="checkbox" name="term">
            <xsl:attribute name="value">
                <xsl:value-of select="current-grouping-key()"/> 
            </xsl:attribute>
            <xsl:attribute name="current_grp_term">
                <xsl:value-of select="current-group()[1]/@term_pattern_code"/>
            </xsl:attribute>
            <xsl:attribute name="url_with_current_val">
                <xsl:value-of select="
                /data/raw/request/h:request/h:requestParameters/h:parameter
                    [@name = 'term']
                    /h:value/text()
                "/>
            </xsl:attribute>
            <xsl:attribute name="local_name">
                <xsl:value-of select="
                    local-name(current-group()[1])
                "/>
            </xsl:attribute>
            <xsl:attribute name="checked">
                <xsl:value-of select="if 
                  (current-group()[1]/@term_pattern_code = 
                  /data/raw/request/h:request/h:requestParameters/h:parameter
                    [@name = local-name(current-group()[1])]
                    /h:value/text())
                  then 'true' else ''"/>
            </xsl:attribute>
           <xsl:value-of select="current-group()[1]"/>
        </input>
        <xsl:if test="position() != last()"><br/></xsl:if>
    </xsl:for-each-group>
  </xsl:template>

</xsl:stylesheet>
