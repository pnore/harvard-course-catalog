<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:import href="common.xsl"/>

  <xsl:template name="content">
    <xsl:choose>
      <xsl:when test="/h:html/h:body">
        <xsl:copy-of select="/h:html/h:body/*"/>
      </xsl:when>
      <xsl:when test="/html/body">
        <xsl:copy-of select="/html/body/*"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="/"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
