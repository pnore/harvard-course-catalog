<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:h="http://apache.org/cocoon/request/2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
 <xsl:output method="xml" 
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-
        transitional.dtd" 
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//
        EN" indent="yes"/>
<!--    <xsl:import href="../common.xsl"/>-->
<!--    I would love to do the sidesearch in a separate modeule, but I ran into caching problems that made development needlessly difficult. Until I understand cocoon caching better, I'm just going to do the sidesearch here. Myer (12/8/2012)
<xsl:import href="sidesearch.xsl"/>
-->
  <xsl:param name="url"/>
    <xsl:variable name="page_title" >
        Department Listing
    </xsl:variable>
  <xsl:param name="querystring"/>
  <xsl:param name="baselink"/>
  <xsl:param name="medium"/>
  <xsl:param name="view"/>
  <xsl:param name="blocknr"/>
  <xsl:param name="dept"/>
  <xsl:param name="cat_num"/>	
  <xsl:variable name="relpath" select="concat($baselink,$medium,'/',$view,'/')"/>
  <xsl:variable name="relpath_crs_grp" select="concat($baselink,$medium,'/course_group/filter?')"/>
  <xsl:variable name="relpath_crs_dtl" select="concat($baselink,$medium,'/course_detail/filter?')"/>
  <xsl:variable name="relpath_dept_lst" select="concat($baselink,$medium,'/course_detail/filter?')"/>
  <xsl:variable name="title" select="'Harvard Course Catalog'"/>
  <xsl:variable name="lastblock" select="number(/data/fas_courses/course[@lastblock='1'][1]/@blocknr)"/>
  <xsl:variable name="myqsparamseq" as="item()*">
    <xsl:sequence select="for $pv in tokenize($querystring,'&amp;') return $pv"/>
  </xsl:variable>
	
	<!-- flag to print map -->
	<xsl:variable name="canPrintMap">
		<xsl:if test="number($cat_num) gt 1 
			and //course[@cat_num=$cat_num]/meeting_locations 
			and (//course[@cat_num=$cat_num]/meeting_locations != '')
			and (normalize-space($building_lat) != '')
			and (normalize-space($building_long) != '') ">
			true
		</xsl:if>
	</xsl:variable>

	
	<xsl:variable name="map-title">
		<xsl:value-of select="/data/fas_courses/course[@cat_num=$cat_num]/title"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="/data/fas_courses/course[@cat_num=$cat_num]/meeting_locations/location/@building"></xsl:value-of>
		<xsl:text>,&#160;Room</xsl:text>
		<xsl:value-of select="/data/fas_courses/course[@cat_num=$cat_num]/meeting_locations/location/@room"></xsl:value-of>        
	</xsl:variable>  
	
	
  <!-- map url parameter codes to dispay names-->
  <xsl:key name="id_to_name_for_term" 
	match="/data/raw/fas_courses/course/term"
	use="@term_pattern_code"/>
  <xsl:key name="id_to_name_for_level" 
	match="/data/raw/fas_courses/course/course_level"
	use="@code"/>
  <xsl:key name="id_to_name_for_course" 
	match="/data/raw/fas_courses/course/title"
	use="../@cat_num"/>
  <xsl:key name="id_to_name_for_department" 
	match="/data/raw/fas_courses/course/department/dept_short_name"
	use="../@code"/>
  <xsl:key name="id_to_name_for_course_group" 
	match="/data/raw/fas_courses/course/course_group"
	use="@code"/>
  <xsl:key name="id_to_name_for_cat_num" 
	match="/data/raw/fas_courses/course/title"
	use="../@cat_num"/>
  <xsl:template match="raw">
      <!-- remove - -->
  </xsl:template>
  <xsl:template match="/">
        <html>
            <head>
                <title><xsl:value-of select="$title"/></title>
                <link rel="stylesheet" 
                    href="{$baselink}css/common.css" 
                    type="text/css" media="screen, projection" />
              <script type="text/javascript" src="{$baselink}js/navy_js.js">&#160;</script>
             </head>
          <body>
                <div id="body_container" class="container box">
                     <xsl:call-template name="top_section"/>
                     <xsl:call-template name="banner"/>
                     <xsl:call-template name="navigation"/>
                     <div id="main">
                       <xsl:call-template name="main"/>                       
                     </div>
                     <!--<xsl:call-template name="footer"/>-->
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template name="top_section">
        <div class="top_section" id="top_section"><img src="{$baselink}img/harvard-logo.gif" width="210" height="32" alt="Harvard_logo" />
        </div>
    </xsl:template>  
    <xsl:template name="banner">
        <div id="banner"><img src="{$baselink}img/header-course-catalog.png" width="462" height="82" alt="harvard_course_catalog_logo" />
        </div>
    </xsl:template>
    <xsl:template name="navigation">
    <div class="navigation" id="navigation_section">	
    <table border="0" cellpadding="0" cellspacing="0" id="layout-table">
    <tr>
    <td valign="top">
    <div id="navigation">
    <ul>
    <li class="first"><a href="{$baselink}">Catalog Home</a></li>				
<!--    <li><a href="{$baselink}contents.html">Catalog Contents</a></li>
    <li><a href="{$baselink}previous.html">Previous Catalogs</a></li>-->
    <li><a href="{$baselink}fo/{$view}/filter?{$querystring}">PDF Version</a></li>
    <li><a href="{$baselink}mobile/{$view}/filter?{$querystring}">Mobile Version</a></li>
    </ul>
    <div style="clear: left;"></div>
    </div>			
    </td>
    </tr>
    </table>
    </div>       
    </xsl:template>  

    <xsl:template name="pagination">
     <h1>
        <xsl:value-of select="$page_title"/>
        <xsl:text> | Page </xsl:text>
        <xsl:value-of select="$blocknr"/>
     </h1> 
     <div id="pagination">
       <ul>
       <xsl:if test="number($blocknr) gt 1">
         <!--<li style="display:inline; padding:15px;">-->
         <li>
            <a href="{concat(
                $relpath,
                number($blocknr)-1,
                '/?',$querystring)}">prev</a>
          </li>
        </xsl:if>
        <!-- invalid slot number for local variable-->
        <!--
        <xsl:for-each select="for $i in 1 to 10 return $i">
        yep
        </xsl:for-each>
        -->
        <xsl:for-each-group 
            select="/data/fas_courses/course/@blocknr" group-by=".">
           <xsl:if test="abs(current-group()[1]-number($blocknr)) lt 5">
              <li>
                <xsl:choose>
                <xsl:when test="current-group()[1]=$blocknr">
                    <xsl:value-of select="current-group()[1]"/>
                </xsl:when>
                <xsl:otherwise>
                <a href="{concat(
                    $relpath
                    ,current-group()[1]
                    ,'/?'
                    ,$querystring)}">
                    <xsl:value-of select="current-group()[1]"/>
                </a>
                </xsl:otherwise>
                </xsl:choose>
               </li>
           </xsl:if>
        </xsl:for-each-group>
        <xsl:if test="number($blocknr) le $lastblock">
        <!-- <li style="display:inline; padding:15px;">-->
          <li>
            <a href="{concat(
                $relpath,
                number($blocknr)+1,'/?',
                $querystring)}">next</a></li>
        </xsl:if>
     </ul>
       
     </div>
     </xsl:template>
    <xsl:template name="main">
        <div id="content">
          <xsl:call-template name="pagination"/>
          <xsl:call-template name="content"/>
        </div>
        <div id="side" style="width:300px" class="container box">
          <div id="viewnav">
            <xsl:call-template name="viewnav"/>
          </div>
          <div id="displaysearchterms">
            <xsl:call-template name="displaysearchterms"/>
          </div>
          <xsl:if test="normalize-space($canPrintMap ) != ''">
	        	<div id="google-map">
	           	 <xsl:call-template name="google-map"/>
	          	</div>
          </xsl:if>
          <div id="sidesearch">
            <xsl:call-template name="sidesearch"/> 
          </div>
        </div>
    </xsl:template>
    <xsl:template name="viewnav">
        <ul style="list-style-type:none;">
        <li>
          <xsl:choose>
            <xsl:when test="$view = 'department_listing'">
              <xsl:text>Department List View</xsl:text>
            </xsl:when>
            <xsl:otherwise>
            <a>
              <xsl:attribute name="href">
                 <xsl:value-of select="
                 concat($baselink,$medium,'/department_listing/filter?',$querystring)"/>
              </xsl:attribute>
              <xsl:text>Department List View</xsl:text>
            </a>
            </xsl:otherwise>
          </xsl:choose>
        </li>
        <li>
          <xsl:choose>
            <xsl:when test="$view = 'course_group'">
              <xsl:text>Course Group View</xsl:text>
            </xsl:when>
            <xsl:otherwise>
            <a>
              <xsl:attribute name="href">
                 <xsl:value-of select="
                 concat($baselink,$medium,'/course_group/filter?',$querystring)"/>
              </xsl:attribute>
              <xsl:text>Course Group View</xsl:text>
            </a>
            </xsl:otherwise>
          </xsl:choose>
        </li>
        <li>
          <xsl:choose>
            <xsl:when test="$view = 'course_detail'">
              <xsl:text>Course Detail View</xsl:text>
            </xsl:when>
            <xsl:otherwise>
            <a>
              <xsl:attribute name="href">
                 <xsl:value-of select="
                 concat($baselink,$medium,'/course_detail/filter?',$querystring)"/>
              </xsl:attribute>
              <xsl:text>Course Detail View</xsl:text>
            </a>
            </xsl:otherwise>
          </xsl:choose>
        </li>
        </ul>
    </xsl:template>
  <xsl:template name="displaysearchterms">
    <xsl:variable name="docroot" select="/"/>
      <div style="border: thin dotted black; padding: 0.2em; margin-top: 1em; margin-bottom: 1em;">
    <p>Search returned
      <xsl:value-of select="count(/data/fas_courses/course)"/> results. </p>
        
          <xsl:if 
            test="count(/data/raw/request/
                h:request/h:requestParameters/h:parameter) > 0">
            <ul style="list-style-type:none;">
            <li>
              <a href="{$relpath_dept_lst}">
                <img src="{$baselink}img/ico-delete.gif" alt="remove"/>
              </a>
              <strong>Remove all search terms</strong>
            </li>
            <xsl:apply-templates select="/data/raw/request/
                h:request/h:requestParameters/h:parameter/h:value"/>
            </ul>
          </xsl:if>
        
      </div>
  </xsl:template>
  <xsl:template match="h:value">
    
    <li>
        <xsl:choose>
          <xsl:when test="../@name='year'">
            <a>
                <xsl:attribute name="href">
                     <xsl:value-of select="$relpath_crs_grp"/>
                     <xsl:choose>
                       <xsl:when
                         test="contains($querystring,concat('&amp;','year='))">
                         <xsl:value-of 
                         select="replace($querystring,concat('&amp;','year=',.),'')"/>
                       </xsl:when>
                       <xsl:otherwise>
                         <xsl:value-of 
                         select="replace($querystring,concat('year=',.),'')"/>
                       </xsl:otherwise>
                     </xsl:choose>   
                </xsl:attribute>
                <img src="{$baselink}img/ico-delete.gif" alt="remove"/>
            </a>
            <strong><xsl:value-of select="."/></strong>
          </xsl:when>
          <xsl:when test="../@name='term'">
            <a>
                <xsl:attribute name="href">
                     <xsl:value-of select="$relpath_crs_grp"/>
                     <xsl:choose>
                       <xsl:when
                         test="contains($querystring,concat('&amp;','term='))">
                         <xsl:value-of 
                         select="replace($querystring,concat('&amp;','term=',.),'')"/>
                       </xsl:when>
                       <xsl:otherwise>
                         <xsl:value-of 
                         select="replace($querystring,concat('term=',.),'')"/>
                       </xsl:otherwise>
                     </xsl:choose>   
                </xsl:attribute>
                <img src="{$baselink}img/ico-delete.gif" alt="remove"/>
            </a>
            <strong><xsl:value-of 
                select="key('id_to_name_for_term',.)[1]"/></strong>
          </xsl:when>
          <xsl:when test="../@name='level'">
            <a>
                <xsl:attribute name="href">
                     <xsl:value-of select="concat(
                            $relpath
                            ,'filter?')"/>
                     <xsl:choose>
                       <xsl:when
                         test="contains($querystring,concat('&amp;','level='))">
                         <xsl:value-of 
                         select="replace($querystring,concat('&amp;','level=',.),'')"/>
                       </xsl:when>
                       <xsl:otherwise>
                         <xsl:value-of 
                         select="replace($querystring,concat('level=',.),'')"/>
                       </xsl:otherwise>
                     </xsl:choose>   
                </xsl:attribute>
                <img src="{$baselink}img/ico-delete.gif" alt="remove"/>
            </a>
            <strong><xsl:text>Level: </xsl:text><xsl:value-of 
                select="key('id_to_name_for_level',.)[1]"/></strong>
          </xsl:when>
          <xsl:when test="../@name='department'">
            <a>
                <xsl:attribute name="href">
                     <xsl:value-of select="concat(
                            $relpath
                            ,'filter?')"/>
                     <xsl:choose>
                       <xsl:when
                         test="contains($querystring,concat('&amp;','department='))">
                         <xsl:value-of 
                         select="replace($querystring,concat('&amp;','department=',.),'')"/>
                       </xsl:when>
                       <xsl:otherwise>
                         <xsl:value-of 
                         select="replace($querystring,concat('department=',.),'')"/>
                       </xsl:otherwise>
                     </xsl:choose>   
                </xsl:attribute>
                <img src="{$baselink}img/ico-delete.gif" alt="remove"/>
            </a>
            <strong><xsl:text>Department: </xsl:text><xsl:value-of 
                select="key('id_to_name_for_department',.)[1]"/></strong>
          </xsl:when>
          <xsl:when test="../@name='course_group'">
            <a>
                <xsl:attribute name="href">
                     <xsl:value-of select="concat(
                            $relpath
                            ,'filter?')"/>
                     <xsl:choose>
                       <xsl:when
                         test="contains($querystring,concat('&amp;','course_group='))">
                         <xsl:value-of 
                         select="replace($querystring,concat('&amp;','course_group=',.),'')"/>
                       </xsl:when>
                       <xsl:otherwise>
                         <xsl:value-of 
                         select="replace($querystring,concat('course_group=',.),'')"/>
                       </xsl:otherwise>
                     </xsl:choose>   
                </xsl:attribute>
                <img src="{$baselink}img/ico-delete.gif" alt="remove"/>
            </a>
            <strong><xsl:text>Course Group: </xsl:text><xsl:value-of 
                select="key('id_to_name_for_course_group',.)[1]"/></strong>
          </xsl:when>
          <xsl:when test="../@name='cat_num'">
            <a>
                <xsl:attribute name="href">
                     <xsl:value-of select="concat(
                            $relpath
                            ,'filter?')"/>
                     <xsl:choose>
                       <xsl:when
                         test="contains($querystring,concat('&amp;','cat_num='))">
                         <xsl:value-of 
                         select="replace($querystring,concat('&amp;','cat_num=',.),'')"/>
                       </xsl:when>
                       <xsl:otherwise>
                         <xsl:value-of 
                         select="replace($querystring,concat('cat_num=',.),'')"/>
                       </xsl:otherwise>
                     </xsl:choose>   
                </xsl:attribute>
                <img src="{$baselink}img/ico-delete.gif" alt="remove"/>
            </a>
            <!--
            no idea why this produces
            Internal error: invalid slot number for local variable (No slot allocated)
            <strong><xsl:value-of select="
                /data/fas_courses
                /course[@cat_num=current()/text()]
                /title"/>        
            </strong>
            -->
            <strong><xsl:text>Course: </xsl:text><xsl:value-of 
                select="key('id_to_name_for_cat_num',.)[1]"/></strong>
          </xsl:when>
        </xsl:choose>
    </li>
  </xsl:template>
  <!-- /render url parameters -->
  <!-- search form on side -->
  <xsl:template name="sidesearch">
    <h4>Search</h4>
    <div id="search_next_year_offered">
    <xsl:call-template name="search_next_year_offered"/>
    </div>
    <div id="search_calendar_period">
    <xsl:call-template name="search_calendar_period"/>
    </div>
    <div id="search_course_level">
    <xsl:call-template name="search_course_level"/>
    </div>
    <div id="search_department">
    <xsl:call-template name="search_department"/>
    </div>
    <xsl:if test="count(/data/raw/request/
    h:request/h:requestParameters/h:parameter[@name='department']) > 0">
        <div id="search_course_group">
        <xsl:call-template name="search_course_group"/>
        </div>
    </xsl:if>
    <xsl:if test="count(
    /data/raw/request/
    h:request/h:requestParameters
    /h:parameter[@name='course_group']) > 0">
        <div id="search_course_title">
        <xsl:call-template name="search_course_title"/>
        </div>
    </xsl:if>
  </xsl:template>
  <xsl:template name="search_next_year_offered">
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Next Year Offered
    </h3>
    <xsl:for-each-group 
      select="/data/fas_courses/course"  
      as="xs:integer"
      group-by="@acad_year|@next_year_offered">
-     <xsl:sort select="current-grouping-key()" as="xs:integer"/>
      <xsl:choose> 
        <xsl:when test="contains(
            $querystring,concat('year=',current-grouping-key()))">
          <xsl:value-of select="current-grouping-key()"/>
        </xsl:when>
        <xsl:otherwise>
          <a>
            <xsl:attribute name="href">
            <xsl:value-of select="$relpath_crs_grp"/>
               <xsl:if test="string-length($querystring) gt 0">
                 <xsl:value-of select="$querystring"/> 
                 <xsl:text>&amp;</xsl:text>
               </xsl:if>
               <xsl:text>year=</xsl:text>
               <xsl:value-of select="current-grouping-key()"/> 
            </xsl:attribute>
            <xsl:value-of select="current-grouping-key()"/>
          </a>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="position() != last()">
          <xsl:text> | </xsl:text>
      </xsl:if>
    </xsl:for-each-group>
  </xsl:template>
  <xsl:template name="search_calendar_period">
    <xsl:variable 
        name="myqs" 
        select="replace($querystring,'cat_num=\d*','')"
        default="''"/>
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Term
    </h3>
    <xsl:for-each-group 
      select="/data/fas_courses/course/term"  
      group-by="@term_pattern_code">
-     <xsl:sort select="current-grouping-key()" data-type="number"/>
      <xsl:variable name="qsparam">
        <xsl:value-of select="concat('term=', current-grouping-key())"/>
      </xsl:variable>
      <xsl:variable name="link_body">
        <xsl:value-of select="current-group()[1]"/>
      </xsl:variable>
      <xsl:choose> 
        <xsl:when test="contains(
            $querystring,concat('term=',current-grouping-key()))">
          <xsl:value-of select="current-group()[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <a>
            <xsl:attribute name="href">
            <xsl:value-of select="$relpath_crs_grp"/>
               <xsl:if test="string-length($querystring) gt 0">
                 <xsl:text>&amp;</xsl:text>
               </xsl:if>
               <xsl:text>term=</xsl:text>
               <xsl:value-of select="current-grouping-key()"/> 
            </xsl:attribute>
            <xsl:value-of select="current-group()[1]" />
          </a>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="position() != last()">
          <xsl:text> | </xsl:text>
      </xsl:if>
    </xsl:for-each-group>
  </xsl:template>
  <xsl:template name="search_course_level">
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Course Level
    </h3>
    <xsl:for-each-group 
      select="/data/fas_courses/course/course_level"  
      group-by="@code">
-     <xsl:sort select="current-grouping-key()"/>
      <xsl:choose> 
        <xsl:when test="contains(
            $querystring,concat('level=',current-grouping-key()))">
          <xsl:value-of select="current-group()[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <a>
            <xsl:attribute name="href">
               <xsl:value-of select="$relpath_crs_grp"/> 
               <xsl:if test="string-length($querystring) gt 0">
                 <xsl:value-of 
                 select="replace($querystring,'level=.','')"/>
                 <xsl:text>&amp;</xsl:text>
               </xsl:if>
               <xsl:text>level=</xsl:text>
               <xsl:value-of select="current-grouping-key()"/> 
            </xsl:attribute>
            <xsl:value-of select="current-group()[1]" />
          </a>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="position() != last()">
          <xsl:text> | </xsl:text>
      </xsl:if>
    </xsl:for-each-group>
  </xsl:template>
  <xsl:template name="search_department">
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Department
    </h3>
    <xsl:for-each-group 
      select="/data/fas_courses/course/department"  
      group-by="@code">
-     <xsl:sort select="current-grouping-key()"/>
      <xsl:choose> 
        <xsl:when test="contains(
            $querystring,concat('department=',current-grouping-key()))">
            <xsl:value-of 
                select="key(
                    'id_to_name_for_department'
                    ,current-grouping-key())[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <a>
            <xsl:attribute name="href">
               <xsl:value-of select="$relpath_crs_grp"/> 
               <xsl:if test="string-length($querystring) gt 0">
                 <xsl:value-of select="$querystring"/> 
                 <xsl:text>&amp;</xsl:text>
               </xsl:if>
               <xsl:text>department=</xsl:text>
               <xsl:value-of select="current-grouping-key()"/> 
            </xsl:attribute>
            <xsl:value-of 
                select="key(
                    'id_to_name_for_department'
                    ,current-grouping-key())[1]"/>
          </a>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="position() != last()">
          <xsl:text> | </xsl:text>
      </xsl:if>
    </xsl:for-each-group>
  </xsl:template>
  <xsl:template name="search_course_group">
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Course Group
    </h3>
    <xsl:for-each-group 
      select="/data/fas_courses/course/course_group"  
      group-by="@code">
-     <xsl:sort select="current-grouping-key()"/>
      <xsl:choose> 
        <xsl:when test="contains(
            $querystring,concat('course_group=',current-grouping-key()))">
          <xsl:value-of select="current-group()[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <a>
            <xsl:attribute name="href">
               <xsl:value-of select="$relpath_crs_grp"/> 
               <xsl:if test="string-length($querystring) gt 0">
                 <xsl:value-of select="$querystring"/> 
                 <xsl:text>&amp;</xsl:text>
               </xsl:if>
               <xsl:text>course_group=</xsl:text>
               <xsl:value-of select="current-grouping-key()"/> 
            </xsl:attribute>
            <xsl:value-of 
                select="key('id_to_name_for_course_group'
                ,current-grouping-key())[1]"/>
          </a>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="position() != last()">
          <xsl:text> | </xsl:text>
      </xsl:if>
    </xsl:for-each-group>
  </xsl:template>
  <xsl:template name="search_course_title">
    <xsl:for-each-group 
      select="/data/raw/fas_courses/course"  
      group-by="course_group/@code">
        <xsl:if test="
        count(/data/raw/request/h:request
        /h:requestParameters/h:parameter[@name='course_group']
        /h:value[current-group()[1]/course_group/@code=text()]) > 0
        ">
        <h3 style="margin-bottom: 2; padding-bottom: 1;">
         <xsl:text>Courses: </xsl:text>
         <xsl:value-of select="current-group()[1]/course_group"/>
        </h3>
      <xsl:for-each select="current-group()">
       <xsl:choose> 
        <!-- when we've selected this course -->
        <xsl:when test="
        count(/data/raw/request/h:request
        /h:requestParameters/h:parameter[@name='cat_num']
        /h:value[@cat_num=text()]) > 0
        ">
          <!-- display just the text of the course name -->
          <xsl:value-of select="title"/>
        </xsl:when>
       <xsl:otherwise>
          <a><!-- display a link -->
            <xsl:attribute name="href">
               <xsl:value-of select="$relpath_crs_dtl"/> 
               <xsl:if test="string-length($querystring) gt 0">
                 <xsl:value-of select="$querystring"/> 
                 <xsl:text>&amp;</xsl:text>
               </xsl:if>
               <xsl:text>cat_num=</xsl:text>
               <xsl:value-of select="@cat_num"/> 
            </xsl:attribute>
            <xsl:value-of select="title"/>
          </a>
      </xsl:otherwise>
     </xsl:choose>
      <xsl:if test="position() != last()">
       <xsl:text> | </xsl:text>
      </xsl:if>
     </xsl:for-each>
     </xsl:if>
    </xsl:for-each-group>
  </xsl:template>
    <xsl:template name="content">
       <h1>Harvard University 2012-13 Course Catalog</h1>
       <p>Welcome to Harvard University's Course Catalog. The searchable catalog lists over 8,000 courses offered at Harvard University from over 100 departments and includes course descriptions, faculty, meeting times, and links to syllabuses and textbook information.</p>
       <p style="margin-bottom: 2em;">We invite you to explore the full breadth and depth of Harvard's academic offerings, searching by calendar period, school, and keyword.</p>
    </xsl:template>
    <xsl:template name="footer">
        <div id="footer">
            <a href="http://validator.w3.org/check/referer">
                <img src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0!"
                    height="31" width="88" />
            </a>
            <a href="http://jigsaw.w3.org/css-validator/check/referer">
                <img src="http://jigsaw.w3.org/css-validator/img/vcss" height="31"
                    width="88" alt="Valid CSS!" />
            </a>
        </div>
    </xsl:template>
  
	<xsl:variable name="building_name">
		<xsl:value-of select="/data/fas_courses/course[@cat_num=$cat_num]/meeting_locations/location/@building"/>
	</xsl:variable>    
	
	<xsl:variable name="building_lat">
		<xsl:value-of select="document('../../data/buildings.xml')/buildings/building/name[text()=$building_name]/../latitude"/>
	</xsl:variable>  
	
	<xsl:variable name="building_long">
		<xsl:value-of select="document('../../data/buildings.xml')/buildings/building/name[text()=$building_name]/../longitude"/>
	</xsl:variable>       
  
	<xsl:template name="google-map">
		<!--
			Course-building:  <xsl:value-of select="$building_name"/><br/>
			Building-info:<xsl:value-of select="$building_lat"/>, <xsl:value-of select="$building_long"/>
		-->
		<script type="text/javascript">
			setClassInfo('<xsl:value-of select="$map-title"/>'); setCoords(<xsl:value-of select="$building_lat"/>, <xsl:value-of select="$building_long"/>); loadScript();
		</script>
		<div id="map_canvas" style="width:100%; height:300px;"></div>
	</xsl:template>  
  
  
</xsl:stylesheet>
  <!-- unused 
  <xsl:template name="search_calendar_period_checkboxes">
    <h3 style="margin-bottom: 5; padding-bottom: 2;">
    Calendar Period
    </h3>
    <xsl:for-each-group 
      select="/data/raw/fas_courses/course/term"  
      group-by="@term_pattern_code">
        <input onclick="this.form.submit()" type="checkbox" name="term">
            <xsl:attribute name="value">
                <xsl:value-of select="current-grouping-key()"/> 
            </xsl:attribute>
            <xsl:attribute name="current_grp_term">
                <xsl:value-of select="current-group()[1]/@term_pattern_code"/>
            </xsl:attribute>
            <xsl:attribute name="url_with_current_val">
                <xsl:value-of select="
                /data/raw/request/h:request/h:requestParameters/h:parameter
                    [@name = 'term']
                    /h:value/text()
                "/>
            </xsl:attribute>
            <xsl:attribute name="local_name">
                <xsl:value-of select="
                    local-name(current-group()[1])
                "/>
            </xsl:attribute>
            <xsl:attribute name="checked">
                <xsl:value-of select="if 
                  (current-group()[1]/@term_pattern_code = 
                  /data/raw/request/h:request/h:requestParameters/h:parameter
                    [@name = local-name(current-group()[1])]
                    /h:value/text())
                  then 'true' else ''"/>
            </xsl:attribute>
           <xsl:value-of select="current-group()[1]"/>
        </input>
        <xsl:if test="position() != last()"><br/></xsl:if>
    </xsl:for-each-group>
  </xsl:template>
  -->
