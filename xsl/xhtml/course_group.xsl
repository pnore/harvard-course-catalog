<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
  xmlns:h="http://apache.org/cocoon/request/2.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
 <xsl:import href="common.xsl"/>
 <xsl:import href="../common.xsl"/>
    <xsl:strip-space elements="*" />
  <xsl:param name="course_group"/>
    <xsl:variable name="courses" select="/data/fas_courses/course"/>
    <xsl:variable name="page_title" >
        Courses in Course Groups
    </xsl:variable>
    <xsl:variable name="crs_grp_course_table_headings">
        <tr>
            <th>Course Number</th>
            <th>PDF</th> 
            <th>Terms Offered</th> 
            <th>Course Title</th>
        </tr>
    </xsl:variable>
   <xsl:template name="content">
   <!--
     <h1>
        <xsl:value-of select="$page_title"/>
        <xsl:text> | Page </xsl:text>
        <xsl:value-of select="$blocknr"/>
     </h1> 
     -->
     <!--
     <div id="pagination">
       <ul>
       <xsl:if test="number($blocknr) gt 1">
         <li>
            <a href="{concat(
                $relpath,
                number($blocknr)-1,
                '/?',$querystring)}">prev</a>
          </li>
        </xsl:if>
        <xsl:for-each-group select="/data/fas_courses/course/@blocknr" group-by=".">
          <li>
            <xsl:choose>
            <xsl:when test="current-group()[1]=$blocknr">
                <xsl:value-of select="current-group()[1]"/>
            </xsl:when>
            <xsl:otherwise>
            <a href="{concat(
                $relpath
                ,current-group()[1]
                ,'/?'
                ,$querystring)}">
                <xsl:value-of select="current-group()[1]"/>
            </a>
            </xsl:otherwise>
            </xsl:choose>
           </li>
        </xsl:for-each-group>
        <xsl:if test="number($blocknr) le $lastblock">
          <li>
            <a href="{concat(
                $relpath,
                number($blocknr)+1,'/?',
                $querystring)}">next</a></li>
        </xsl:if>
     </ul>
       
     </div>
     -->
     
      <!--<xsl:for-each-group 
        select="/data/fas_courses/course/course_group" 
        group-by="@code">
        <xsl:if test="last() gt 1">
          <a href="#course_group-{current-grouping-key()}">
            <xsl:value-of select="."/>
          </a>
          <xsl:if test="position() != last()">
            <xsl:text> | </xsl:text>
          </xsl:if>
          </xsl:if>
      </xsl:for-each-group>-->
     <br/> 
      <xsl:for-each-group 
        select="/data/fas_courses/course[@blocknr=$blocknr]" 
        group-by="course_group/@code">
        <h2 id="{concat('course_group-',current-grouping-key())}">
            <xsl:value-of select="current-group()[1]/course_group"/>
        </h2>
        <table>
          <xsl:copy-of select="$crs_grp_course_table_headings" />
          <xsl:for-each select="current-group()">
            <xsl:apply-templates select="." mode="table-row"/>
          </xsl:for-each>
        </table>
      </xsl:for-each-group>
      
  </xsl:template>
  <xsl:template match="course" mode="table-row">
    <tr>
        <td><xsl:apply-templates select="course_number"/></td>
        <td><xsl:call-template name="pdf-link"></xsl:call-template></td>
        <td><xsl:apply-templates select="term"/></td>
        <td><xsl:apply-templates select="title"/></td>
    </tr>
  </xsl:template>
  <xsl:template match="course_number">
  <!-- old
    <a href="{concat('course-',../@cat_num,'.html')}">
        <xsl:apply-templates mode="common" select="."/>
    </a>
    -->
    <a>
      <xsl:attribute name="href">
         <xsl:value-of select="
         concat($baselink,$medium,
         '/course_detail/filter?',$querystring,'&amp;','cat_num=',
         ../@cat_num 
         )"/>
      </xsl:attribute>
      <xsl:apply-templates mode="common" select="."/>
    </a>
  </xsl:template>
  
  <xsl:template name="pdf-link">
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="
          concat($baselink,'fo',
          '/course_detail/filter?',$querystring,'&amp;','cat_num=',
          @cat_num 
          )"/>
      </xsl:attribute>
      <img src="{$baselink}img/pdf-icon.png" alt="remove"/>            
    </a>     
  </xsl:template>
  
</xsl:stylesheet>
